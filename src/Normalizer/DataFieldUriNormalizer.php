<?php

namespace Drupal\datafield\Normalizer;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\serialization\Normalizer\PrimitiveDataNormalizer;

/**
 * Normalizes/denormalizes uri field data.
 */
class DataFieldUriNormalizer extends PrimitiveDataNormalizer {

  /**
   * Constructor of Subfield Uri Normalizer object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository service.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager, protected EntityRepositoryInterface $entityRepository) {
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [
      ComplexDataInterface::class => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|NULL {
    if (method_exists($this, 'selfSupportsNormalization') && $this->selfSupportsNormalization($object, $format, $context)) {
      return parent::normalize($object, $format, $context);
    }
    if (!empty($this->fallbackNormalizer) && $this->fallbackNormalizer->supportsNormalization($object, $format, $context)) {
      return $this->fallbackNormalizer->normalize($object, $format, $context);
    }
    return parent::normalize($object, $format, $context);
  }

}
