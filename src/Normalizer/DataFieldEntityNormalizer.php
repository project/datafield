<?php

namespace Drupal\datafield\Normalizer;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\serialization\Normalizer\FieldItemNormalizer;

/**
 * Normalizes/denormalizes specific field data.
 */
class DataFieldEntityNormalizer extends FieldItemNormalizer {

  /**
   * {@inheritdoc}
   */
  protected $format = ['json', 'xml'];

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = FieldItemInterface::class;

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [
      ComplexDataInterface::class => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|NULL {
    if (method_exists($this, 'selfSupportsNormalization') && $this->selfSupportsNormalization($object, $format, $context)) {
      return parent::normalize($object, $format, $context);
    }
    if (!empty($this->fallbackNormalizer) && $this->fallbackNormalizer->supportsNormalization($object, $format, $context)) {
      return $this->fallbackNormalizer->normalize($object, $format, $context);
    }
    return parent::normalize($object, $format, $context);
  }

}
