<?php
// phpcs:ignoreFile
namespace Drupal\datafield\Annotation;

use Drupal\Core\TypedData\Annotation\DataType;

/**
 * Defines a Data FieldType annotation object.
 *
 * Additional annotation keys for field types can be defined in
 * hook_field_info_alter().
 *
 * @ingroup field_types
 *
 * @Annotation
 */
class DataFieldType extends DataType {

  /**
   * The name of the module providing the field type plugin.
   *
   * @var string
   */
  public string $module;

  /**
   * The category under which the field type should be listed in the UI.
   *
   * @var string
   */
  public string $category = '';

  /**
   * The plugin_id of the default widget for this field type.
   *
   * This widget must be available whenever the field type is available (i.e.
   * provided by the field type module, or by a module the field type module
   * depends on).
   *
   * @var string
   */
  public string $default_widget;

  /**
   * The plugin_id of the default formatter for this field type.
   *
   * This formatter must be available whenever the field type is available (i.e.
   * provided by the field type module, or by a module the field type module
   * depends on).
   *
   * @var string
   */
  public string $default_formatter;

  /**
   * A boolean stating that fields of this type can't be created through the UI.
   *
   * @var bool
   */
  public bool $no_ui = FALSE;

  /**
   * An integer defining a fixed cardinality for this field type.
   *
   * If this value is not set, cardinality can be configured in the field UI.
   *
   * @var int|null
   */
  public ?int $cardinality;

}
