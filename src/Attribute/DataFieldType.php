<?php

namespace Drupal\datafield\Attribute;

use Drupal\Core\Field\Attribute\FieldType;

/**
 * The data field type attribute.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class DataFieldType extends FieldType {
}
