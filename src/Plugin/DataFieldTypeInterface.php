<?php

namespace Drupal\datafield\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for the field type plugin manager.
 *
 * @ingroup field_types
 */
interface DataFieldTypeInterface extends PluginInspectionInterface {

  /**
   * Schema.
   *
   * {@inheritdoc}
   */
  public static function schema(array $settings);

  /**
   * Get label data type.
   *
   * {@inheritdoc}
   */
  public function getLabel();

  /**
   * Default storage settings.
   *
   * {@inheritdoc}
   */
  public static function defaultStorageSettings();

  /**
   * Render sample values.
   *
   * {@inheritdoc}
   */
  public static function generateSampleValue();

  /**
   * Field settings form.
   *
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $field_settings, array $settings);

  /**
   * {@inheritdoc}
   */
  public function getConstraints(array $settings);

  /**
   * Property definitions.
   *
   * {@inheritdoc}
   */
  public static function propertyDefinitions(array $settings);

}
