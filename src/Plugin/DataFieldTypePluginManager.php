<?php

namespace Drupal\datafield\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\datafield\Attribute\DataFieldType;

/**
 * Plugin manager for 'field type' plugins.
 *
 * @ingroup field_types
 */
class DataFieldTypePluginManager extends DefaultPluginManager {

  /**
   * Constructs the FieldTypePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/DataField/FieldType', $namespaces, $module_handler,
      'Drupal\datafield\Plugin\DataFieldTypeInterface',
      DataFieldType::class,
      'Drupal\datafield\Annotation\DataFieldType'
    );
    $this->alterInfo($this->getType());
    $this->setCacheBackend($cache_backend, 'data_field_types_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function getType() {
    return 'data_field_type_info';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginClass($type) {
    $definition = $this->getDefinition($type);
    if (!isset($definition['class'])) {
      throw new PluginException(sprintf('Class not defined for plugin type "%s".', $type));
    }
    return $definition['class'];
  }

}
