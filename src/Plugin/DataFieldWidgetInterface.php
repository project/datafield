<?php

namespace Drupal\datafield\Plugin;

/**
 * Interface definition for field widget plugins.
 *
 * This interface details the methods that most plugin implementations will want
 * to override. See Drupal\Core\Field\WidgetBaseInterface for base
 * wrapping methods that should most likely be inherited directly from
 * Drupal\Core\Field\WidgetBase..
 *
 * @ingroup field_widget
 */
interface DataFieldWidgetInterface {

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item, $setting);

}
