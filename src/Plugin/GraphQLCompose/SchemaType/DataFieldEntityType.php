<?php

namespace Drupal\datafield\Plugin\GraphQLCompose\SchemaType;

use Drupal\graphql_compose\Plugin\GraphQLCompose\GraphQLComposeSchemaTypeBase;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

/**
 * {@inheritDoc}
 */
#[GraphQLComposeSchemaType(
  id: 'DataFieldEntityType',
)]
class DataFieldEntityType extends GraphQLComposeSchemaTypeBase {

  /**
   * {@inheritdoc}
   */
  public function getTypes(): array {
    $types = [];

    $types[] = new ObjectType([
      'name' => $this->getPluginId(),
      'description' => (string) $this->t('An entity object.'),
      'fields' => fn() => [
        'uuid' => [
          'type' => Type::string(),
          'description' => (string) $this->t('The entity Universally Unique Identifier of the entity.'),
        ],
        'id' => [
          'type' => Type::nonNull(Type::int()),
          'description' => (string) $this->t('The entity id of the entity.'),
        ],
        'name' => [
          'type' => Type::string(),
          'description' => (string) $this->t('The title of the entity.'),
        ],
        'url' => [
          'type' => Type::nonNull(Type::string()),
          'description' => (string) $this->t('The URL of the entity.'),
        ],
        'type' => [
          'type' => Type::string(),
          'description' => (string) $this->t('The entity type.'),
        ],
        'bundle' => [
          'type' => Type::string(),
          'description' => (string) $this->t('The bundle of the entity.'),
        ],
        'description' => [
          'type' => static::type('Html'),
          'description' => (string) $this->t('The description of the entity.'),
        ],
        'data' => [
          'type' => Type::listOf(Type::string()),
          'description' => (string) $this->t('The data of entity.'),
        ],
      ],
    ]);
    return $types;
  }

}
