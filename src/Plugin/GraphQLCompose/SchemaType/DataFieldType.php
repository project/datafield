<?php

namespace Drupal\datafield\Plugin\GraphQLCompose\SchemaType;

use Drupal\datafield\Plugin\GraphQLCompose\FieldType\DataFieldItem;
use Drupal\graphql_compose\Plugin\GraphQLCompose\GraphQLComposeSchemaTypeBase;
use GraphQL\Type\Definition\ObjectType;

/**
 * {@inheritDoc}
 */
#[GraphQLComposeSchemaType(
  id: 'DataField',
)]
class DataFieldType extends GraphQLComposeSchemaTypeBase {

  /**
   * {@inheritdoc}
   */
  public function getTypes(): array {
    $types = [];

    if (!$this->moduleHandler->moduleExists('datafield')) {
      return $types;
    }

    // Find all exposed data fields.
    // This assumes all fields have been bootstrapped.
    $fields = $this->gqlFieldTypeManager->getFields();

    array_walk_recursive($fields, function ($field) use (&$types) {
      if ($field instanceof DataFieldItem) {
        $types[$field->getTypeSdl()] = new ObjectType([
          'name' => $field->getTypeSdl(),
          'description' => (string) $this->t('A data field is a specialty field provided by the CMS.'),
          'fields' => function () use ($field) {
            $columns = array_keys($field->getFieldDefinition()->getSetting('columns'));
            $subFields = [];
            foreach ($columns as $column) {
              $subFields[$column] = [
                'type' => static::type($field->getSubfieldTypeSdl($column)),
                'description' => (string) $this->t('The @field value of the data field', ['@field' => $column]),
              ];
            }
            return $subFields;
          },
        ]);
      }
    });

    return array_values($types);
  }

}
