<?php

namespace Drupal\datafield\Plugin\GraphQLCompose\FieldType;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql_compose\Plugin\GraphQL\DataProducer\FieldProducerItemInterface;
use Drupal\graphql_compose\Plugin\GraphQL\DataProducer\FieldProducerTrait;
use Drupal\graphql_compose\Plugin\GraphQLCompose\GraphQLComposeFieldTypeBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritDoc}
 */
#[GraphQLComposeFieldType(
  id: 'data_entity_reference',
  type_sdl: 'DataFieldEntityType',
)]
class DataFieldEntityItem extends GraphQLComposeFieldTypeBase implements FieldProducerItemInterface, ContainerFactoryPluginInterface {

  use FieldProducerTrait;

  /**
   * Drupal renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->renderer = $container->get('renderer');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveFieldItem(FieldItemInterface $item, FieldContext $context) {
    if (!$item->entity) {
      return NULL;
    }

    $access = $item->entity->access('view', NULL, TRUE);
    $context->addCacheableDependency($access);

    if (!$access->isAllowed()) {
      return NULL;
    }
    $entity = $item->entity;
    $render_context = new RenderContext();
    $url = $entity->hasLinkTemplate('canonical') ? $entity->toUrl()->toString() : '';
    $label_field = $entity->getEntityType()->getKey('label');
    $title = $label_field ? $entity->get($label_field)?->value : '';
    if (empty($title) && method_exists($entity, 'getDisplayName')) {
      $title = $entity->getDisplayName();
    }
    if (!$render_context->isEmpty()) {
      $context->addCacheableDependency($render_context->pop());
    }
    $data = [];
    $field_definitions = $entity->getFieldDefinitions();
    foreach ($field_definitions as $field_name => $field) {
      if ($field instanceof FieldConfigInterface) {
        $data[$field_name] = $entity->$field_name->value;
      }
    }
    $context->addCacheableDependency($entity);
    return [
      'url' => $url,
      'uuid' => $entity->uuid(),
      'id' => $entity->id(),
      'type' => $entity->getEntityTypeId() ?? NULL,
      'bundle' => $entity->bundle() ?? NULL,
      'name' => $title,
      'description' => $item->description ?: NULL,
      'data' => $data,
    ];
  }

}
