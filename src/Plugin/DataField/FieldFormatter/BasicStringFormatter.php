<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;

/**
 * Plugin implementation of the 'basic_string' formatter.
 */
#[FieldFormatter(
  id: 'basic_string',
  label: new TranslatableMarkup('Format text'),
  field_types: ['string', 'text', 'blob'],
)]
class BasicStringFormatter implements DataFieldFormatterInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    if (is_null($item->value)) {
      return $item->value;
    }
    $elements = [
      '#type' => 'inline_template',
      '#template' => '{{ value|nl2br }}',
      '#context' => ['value' => $item->value],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    return [];
  }

}
