<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;

/**
 * Plugin implementation of the 'boolean' formatter.
 */
#[FieldFormatter(
  id: 'boolean',
  label: new TranslatableMarkup('Boolean'),
  field_types: ['boolean'],
)]
class BooleanFormatter implements DataFieldFormatterInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = [];

    // Fall back to field settings by default.
    $settings['format'] = 'yes-no';
    $settings['format_custom_false'] = '';
    $settings['format_custom_true'] = '';

    return $settings;
  }

  /**
   * Gets the available format options.
   *
   * @return array|string
   *   A list of output formats. Each entry is keyed by the machine name of the
   *   format. The value is an array, of which the first item is the result for
   *   boolean TRUE, the second is for boolean FALSE. The value can be also an
   *   array, but this is just the case for the custom format.
   */
  protected function getOutputFormats() {
    $formats = [
      'yes-no' => [$this->t('Yes'), $this->t('No')],
      'true-false' => [$this->t('True'), $this->t('False')],
      'on-off' => [$this->t('On'), $this->t('Off')],
      'enabled-disabled' => [$this->t('Enabled'), $this->t('Disabled')],
      'boolean' => [1, 0],
      'unicode-yes-no' => ['✔', '✖'],
      'custom' => $this->t('Custom'),
    ];

    return $formats;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $formats = [];
    foreach ($this->getOutputFormats() as $format_name => $format) {
      if (is_array($format)) {
        if ($format_name == 'default') {
          $formats[$format_name] = $this->t('Field settings (@on_label / @off_label)', [
            '@on_label' => $format[0],
            '@off_label' => $format[1],
          ]);
        }
        else {
          $formats[$format_name] = $this->t('@on_label / @off_label', [
            '@on_label' => $format[0],
            '@off_label' => $format[1],
          ]);
        }
      }
      else {
        $formats[$format_name] = $format;
      }
    }

    $settings = $form['#settings'];
    $subFieldName = $form['#machine_name'];
    $element['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Output format'),
      '#default_value' => $settings['format'] ?? self::defaultSettings()['format'],
      '#options' => $formats,
      '#empty_option' => $this->t('- Select -'),
    ];
    $visible = 'select[name*="[settings_edit_form][settings][formatter_settings][' . $subFieldName . '][format]"]';
    $element['format_custom_true'] = [
      '#type' => 'textfield',
      '#title' => $this->t('True label'),
      '#default_value' => $settings['format_custom_true'] ?? self::defaultSettings()['format_custom_true'],
      '#states' => [
        'visible' => [$visible => ['value' => 'custom']],
      ],
    ];
    $element['format_custom_false'] = [
      '#type' => 'textfield',
      '#title' => $this->t('False label'),
      '#default_value' => $settings['format_custom_false'] ?? self::defaultSettings()['format_custom_false'],
      '#states' => [
        'visible' => [$visible => ['value' => 'custom']],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $setting = $settings['format'];
    $formats = $this->getOutputFormats();
    $true = $settings["on_label"];
    $false = $settings["off_label"];
    if (!empty($formats[$setting]) && $setting != 'custom') {
      $true = $formats[$setting][0];
      $false = $formats[$setting][1];
    }
    $summary[] = $this->t('Display: @true_label / @false_label', [
      '@true_label' => $true,
      '@false_label' => $false,
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    $formats = $this->getOutputFormats();
    $format = $item->settings['format'] ?? 'yes-no';
    if ($format == 'custom') {
      $elements = ['#markup' => $item->value ? $item->settings['format_custom_true'] : $item->settings['format_custom_false']];
    }
    else {
      $elements = ['#markup' => $item->value ? $formats[$format][0] : $formats[$format][1]];
    }
    return $elements;
  }

}
