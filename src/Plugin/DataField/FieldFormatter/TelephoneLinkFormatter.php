<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;

/**
 * Plugin implementation of the 'telephone_link' formatter.
 */
#[FieldFormatter(
  id: 'telephone_link',
  label: new TranslatableMarkup('Telephone link'),
  field_types: ['telephone'],
)]
class TelephoneLinkFormatter implements DataFieldFormatterInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'title' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $form['#settings'];
    $elements['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title to replace basic numeric telephone number display'),
      '#default_value' => $settings['title'] ?? '',
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    if (!empty($settings['title'])) {
      $summary[] = $this->t('Link using text: @title', ['@title' => $settings['title']]);
    }
    else {
      $summary[] = $this->t('Link using provided telephone number.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    if (is_null($item->value)) {
      return $item->value;
    }
    $title_setting = $item->settings['title'] ?? FALSE;
    $phone_number = preg_replace('/\s+/', '', $item->value);
    if (strlen($phone_number) <= 5) {
      $phone_number = substr_replace($phone_number, '-', 1, 0);
    }
    // Render each element as link.
    return [
      '#type' => 'link',
      '#title' => $title_setting ?: $item->value,
      '#url' => Url::fromUri('tel:' . rawurlencode($phone_number)),
      '#options' => ['external' => TRUE],
    ];
  }

}
