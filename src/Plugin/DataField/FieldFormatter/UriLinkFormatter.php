<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'uri_link' formatter.
 */
#[FieldFormatter(
  id: 'uri_link',
  label: new TranslatableMarkup('Link to URI'),
  field_types: ['uri', 'string'],
)]
class UriLinkFormatter implements DataFieldFormatterInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a UriLinkFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   The alias manager.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected EntityTypeManagerInterface $entityTypeManager, protected AliasManagerInterface $aliasManager) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('entity_type.manager'),
      $container->get('path_alias.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    if (empty($item->value)) {
      return $item->value;
    }
    if (is_numeric($item->value)) {
      $entity_type = 'node';
      $entity = $this->entityTypeManager->getStorage($entity_type)
        ->load($item->value);
      $url = $entity->toUrl();
      $title = $entity->label();
      return [
        '#type' => 'link',
        '#url' => $url,
        '#title' => $title,
        '#langcode' => $langcode,
      ];
    }
    elseif (str_starts_with($item->value, 'http')) {
      $url = Url::fromUri($item->value);
      return [
        '#type' => 'link',
        '#url' => $url,
        '#title' => $item->value,
        '#langcode' => $langcode,
      ];
    }
    elseif (str_starts_with($item->value, '/')) {
      $url = Url::fromUri('internal:' . $item->value);
      if ($url->isRouted()) {
        $params = $url->getRouteParameters();
        $entity_type = key($params);
        $entity = $this->entityTypeManager->getStorage($entity_type)->load($params[$entity_type]);
        $url = $entity->toUrl();
        $title = $entity->label();
        return [
          '#type' => 'link',
          '#url' => $url,
          '#title' => $title,
          '#langcode' => $langcode,
        ];
      }
    }
    $alias = $this->aliasManager->getAliasByPath($item->value, $langcode);
    $url = Url::fromUserInput($alias);
    return [
      '#type' => 'link',
      '#url' => $url,
      '#title' => $item->value,
      '#langcode' => $langcode,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    return [];
  }

}
