<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;

/**
 * Plugin implementation of the 'number_unformatted' formatter.
 */
#[FieldFormatter(
  id: 'number_unformatted',
  label: new TranslatableMarkup('Unformatted'),
  field_types: ['decimal', 'numeric', 'float'],
)]
class NumericUnformattedFormatter implements DataFieldFormatterInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'thousand_separator' => '',
      'decimal_separator' => '.',
      'scale' => 2,
      'prefix_suffix' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    $value = !is_null($item->value) ? $item->value : $item->value_original;
    $output = $this->numberFormat($value, $item->settings);

    // Account for prefix and suffix.
    $settings = $item->field_setting;
    if (!empty($item->settings["prefix_suffix"])) {
      $prefixes = isset($settings['prefix']) ? array_map([
        'Drupal\Core\Field\FieldFilteredMarkup',
        'create',
      ], explode('|', $settings['prefix'])) : [''];
      $suffixes = isset($settings['suffix']) ? array_map([
        'Drupal\Core\Field\FieldFilteredMarkup',
        'create',
      ], explode('|', $settings['suffix'])) : [''];
      $prefix = (count($prefixes) > 1) ? $this->formatPlural($value, $prefixes[0], $prefixes[1]) : $prefixes[0];
      $suffix = (count($suffixes) > 1) ? $this->formatPlural($value, $suffixes[0], $suffixes[1]) : $suffixes[0];
      $output = $prefix . $output . $suffix;
    }
    $element = ['#markup' => $output, '#value' => $value];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function numberFormat($number, $setting) {
    if (!is_numeric($number) || empty($number)) {
      return $number;
    }
    $setting += self::defaultSettings();
    return number_format($number, $setting['scale'], $setting['decimal_separator'], $setting['thousand_separator']);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $form['#settings'];
    $elements['decimal_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Decimal marker'),
      '#options' => ['.' => $this->t('Decimal point'), ',' => $this->t('Comma')],
      '#default_value' => $settings['decimal_separator'] ?? self::defaultSettings()['decimal_separator'],
    ];
    $elements['scale'] = [
      '#type' => 'number',
      '#title' => $this->t('Scale', [], ['context' => 'decimal places']),
      '#min' => 0,
      '#max' => 10,
      '#default_value' => $settings['scale'] ?? self::defaultSettings()['scale'],
      '#description' => $this->t('The number of digits to the right of the decimal.'),
    ];
    $options = [
      '.' => $this->t('Decimal point'),
      ',' => $this->t('Comma'),
      ' ' => $this->t('Space'),
      chr(8201) => $this->t('Thin space'),
      "'" => $this->t('Apostrophe'),
    ];
    $elements['thousand_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Thousand marker'),
      '#options' => $options,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $settings['thousand_separator'] ?? self::defaultSettings()['thousand_separator'],
    ];
    $elements['prefix_suffix'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display prefix and suffix'),
      '#default_value' => $settings['prefix_suffix'] ?? self::defaultSettings()['prefix_suffix'],
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary[] = $this->t('Number format: @format', [
      '@format' => number_format(1234.1234567890,
        $settings['scale'] ?? self::defaultSettings()['scale'],
        $settings['decimal_separator'] ?? self::defaultSettings()['decimal_separator'],
        $settings['thousand_separator'] ?? self::defaultSettings()['thousand_separator'],
      ),
    ]);
    return $summary;
  }

}
