<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 */
#[FieldFormatter(
  id: 'entity_reference_entity_view',
  label: new TranslatableMarkup('Rendered entity'),
  description: new TranslatableMarkup('Display the referenced entities rendered by entity_view().'),
  field_types: ['entity_reference'],
)]
class EntityReferenceEntityFormatter implements DataFieldFormatterInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a EntityReferenceEntityFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   The entity display service.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected EntityTypeManagerInterface $entityTypeManager, protected readonly EntityDisplayRepositoryInterface $entityDisplayRepository) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
    );
  }

  /**
   * The number of times this formatter allows rendering the same entity.
   *
   * @var int
   */
  const RECURSIVE_RENDER_LIMIT = 20;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'view_mode' => 'default',
      'link' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $form['#settings'];
    $extract = explode(':', $form["#field_settings"]["entity_reference_type"] ?? '');
    $options = $this->entityDisplayRepository->getViewModeOptions(end($extract));
    if (empty($form["#field_settings"])) {
      $all_view_modes = $this->entityDisplayRepository->getAllViewModes();
      foreach ($all_view_modes as $view_modes) {
        foreach ($view_modes as $mode => $view_mode) {
          $options[$mode] = $view_mode['label'];
        }
      }
    }

    $elements['view_mode'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('View mode'),
      '#default_value' => $settings['view_mode'] ?? self::defaultSettings()['view_mode'],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $view_mode = $settings['view_mode'] ?? '';
    $summary[] = $this->t('Rendered as @mode', ['@mode' => $view_mode]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    $view_mode = !empty($item->settings['view_mode']) ? $item->settings['view_mode'] : 'full';
    $entity_reference_type = $item->field_setting["entity_reference_type"] ?? '';
    $explode = explode(':', $entity_reference_type);
    $reference_type = end($explode);
    if (empty($reference_type) || empty($item->value)) {
      return $item->value;
    }
    $entity = $this->entityTypeManager->getStorage($reference_type)->load($item->value);
    if (empty($entity)) {
      return $item->value;
    }
    $view_builder = $this->entityTypeManager->getViewBuilder($reference_type);
    return $view_builder->view($entity, $view_mode);

  }

}
