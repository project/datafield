<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'file_table' formatter.
 */
#[FieldFormatter(
  id: 'file_table',
  label: new TranslatableMarkup('Table of files'),
  field_types: ['file'],
)]
class TableFileFormatter implements DataFieldFormatterInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a TableFileFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\file\FileUsage\FileUsageInterface $fileUsage
   *   The file usage service.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected EntityTypeManagerInterface $entityTypeManager, protected readonly FileUsageInterface $fileUsage) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('entity_type.manager'),
      $container->get('file.usage'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    $fid = $item->value;
    if (!is_numeric($fid)) {
      return $item->value;
    }
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    $header = [$this->t('Attachment'), $this->t('Size')];
    $usage = $this->fileUsage->listUsage($file);
    if (empty($usage)) {
      $entity = $item->entity ?? $item->getEntity();
      $this->fileUsage->add($file, 'datafield', $entity->getEntityTypeId(), $entity->id());
    }
    $rows[] = [
      [
        'data' => [
          '#theme' => 'file_link',
          '#file' => $file,
          '#cache' => ['tags' => $file->getCacheTags()],
        ],
      ],
      ['data' => ByteSizeMarkup::create($file->getSize())],
    ];
    return [
      '#theme' => 'table__file_formatter_table',
      '#header' => $header,
      '#rows' => $rows,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $form['#settings'];
    $element['file_download_path'] = [
      '#title' => $this->t('Display the file download URI'),
      '#type' => 'checkbox',
      '#default_value' => $settings['file_download_path'] ?? FALSE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    if (!empty($settings['file_download_path'])) {
      $summary[] = $this->t('Link formatting table');
    }
    return $summary;
  }

}
