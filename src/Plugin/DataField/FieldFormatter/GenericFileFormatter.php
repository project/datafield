<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'file_default' formatter.
 */
#[FieldFormatter(
  id: 'file_default',
  label: new TranslatableMarkup('Generic file'),
  field_types: ['file'],
)]
class GenericFileFormatter implements DataFieldFormatterInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a TableFileFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\file\FileUsage\FileUsageInterface $fileUsage
   *   The file usage service.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected EntityTypeManagerInterface $entityTypeManager, protected readonly FileUsageInterface $fileUsage) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('entity_type.manager'),
      $container->get('file.usage'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings['use_description_as_link_text'] = TRUE;
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $form['#settings'];
    $elements['use_description_as_link_text'] = [
      '#title' => $this->t('Use description as link text'),
      '#description' => $this->t('Replace the file name by its description when available'),
      '#type' => 'checkbox',
      '#default_value' => $settings['use_description_as_link_text'] ?? TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    if (!empty($settings['use_description_as_link_text'])) {
      $summary[] = $this->t('Use description as link text');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    if (empty($item->value) || !is_numeric($item->value)) {
      return $item->value;
    }
    $fid = $item->value;
    if (empty($fid)) {
      return $item->value;
    }
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (empty($file)) {
      return $item->value;
    }
    $usage = $this->fileUsage->listUsage($file);
    if (empty($usage)) {
      $entity = $item->entity ?? $item->getEntity();
      $this->fileUsage->add($file, 'datafield', $entity->getEntityTypeId(), $entity->id());
    }
    return [
      '#theme' => 'file_link',
      '#file' => $file,
      '#description' => !empty($item->description) ? $item->description : NULL,
      '#cache' => ['tags' => $file->getCacheTags()],
    ];
  }

}
