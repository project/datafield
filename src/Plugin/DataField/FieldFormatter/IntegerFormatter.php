<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;

/**
 * Plugin implementation of the 'number_integer' formatter.
 *
 * The 'Default' formatter is different for integer fields on the one hand, and
 * for decimal and float fields on the other hand, in order to be able to use
 * different settings.
 */
#[FieldFormatter(
  id: 'number_integer',
  label: new TranslatableMarkup('Default'),
  field_types: ['integer'],
)]
class IntegerFormatter implements DataFieldFormatterInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'thousand_separator' => '',
      'prefix_suffix' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function numberFormat($number = 0, $settings = []) {
    if (!is_numeric($number) || empty($number)) {
      return $number;
    }
    return number_format($number, 0, '', $settings['thousand_separator'] ?? '');
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $form['#settings'];
    $options = [
      '.' => $this->t('Decimal point'),
      ',' => $this->t('Comma'),
      ' ' => $this->t('Space'),
      chr(8201) => $this->t('Thin space'),
      "'" => $this->t('Apostrophe'),
    ];
    $elements['thousand_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Thousand marker'),
      '#options' => $options,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $settings['thousand_separator'] ?? self::defaultSettings()['thousand_separator'],
    ];
    $elements['prefix_suffix'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display prefix and suffix'),
      '#default_value' => $settings['prefix_suffix'] ?? self::defaultSettings()['prefix_suffix'],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary[] = $this->t('Number format: @format', [
      '@format' => $this->numberFormat(1234567890, $settings),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    $output = $this->numberFormat($item->value, $item->settings);
    $settings = $item->field_setting;
    if (!empty($item->settings["prefix_suffix"])) {
      $value = !is_null($item->value) ? $item->value : $item->value_original;
      $prefixes = isset($settings['prefix']) ? array_map([
        'Drupal\Core\Field\FieldFilteredMarkup',
        'create',
      ], explode('|', $settings['prefix'])) : [''];
      $suffixes = isset($settings['suffix']) ? array_map([
        'Drupal\Core\Field\FieldFilteredMarkup',
        'create',
      ], explode('|', $settings['suffix'])) : [''];
      $prefix = (count($prefixes) > 1) ? $this->formatPlural($value, $prefixes[0], $prefixes[1]) : $prefixes[0];
      $suffix = (count($suffixes) > 1) ? $this->formatPlural($value, $suffixes[0], $suffixes[1]) : $suffixes[0];
      $output = $prefix . $output . $suffix;
    }
    return $output;
  }

}
