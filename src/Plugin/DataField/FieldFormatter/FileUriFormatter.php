<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Formatter to render the file URI to its download path.
 */
#[FieldFormatter(
  id: 'file_uri',
  label: new TranslatableMarkup('File URI'),
  field_types: ['uri', 'file'],
)]
class FileUriFormatter implements DataFieldFormatterInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a TableFileFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\file\FileUsage\FileUsageInterface $fileUsage
   *   The file usage service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file url generator.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected EntityTypeManagerInterface $entityTypeManager, protected readonly FileUsageInterface $fileUsage, protected readonly FileUrlGeneratorInterface $fileUrlGenerator) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('entity_type.manager'),
      $container->get('file.usage'),
      $container->get('file_url_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings['file_download_path'] = FALSE;
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $form['#settings'];
    $element['file_download_path'] = [
      '#title' => $this->t('Display the file download URI'),
      '#type' => 'checkbox',
      '#default_value' => $settings['file_download_path'] ?? FALSE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $settings += self::defaultSettings();
    if ($settings['file_download_path']) {
      $summary[] = $this->t('Display the file download URI');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    if (empty($item->value) || !is_numeric($item->value)) {
      return $item->value;
    }
    $fid = $item->value;
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (empty($file)) {
      return $item->value;
    }
    $url = $this->fileUrlGenerator->generate($file->getFileUri());
    $fieldName = $file->getFilename();
    $ext = pathinfo($fieldName, PATHINFO_EXTENSION);

    $title = new FormattableMarkup('<i class="bi bi-filetype-@ext"></i> ' . $fieldName, [
      '@ext' => strtolower($ext),
    ]);
    $usage = $this->fileUsage->listUsage($file);
    if (empty($usage)) {
      $entity = $item->entity ?? $item->getEntity();
      $this->fileUsage->add($file, 'datafield', $entity->getEntityTypeId(), $entity->id());
    }
    return [
      '#type' => 'link',
      '#langcode' => $langcode,
      '#title' => $title,
      '#url' => $url,
      '#cache' => ['tags' => $file->getCacheTags()],
    ];
  }

}
