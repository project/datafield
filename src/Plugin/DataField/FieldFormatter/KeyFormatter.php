<?php

namespace Drupal\datafield\Plugin\DataField\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldFormatterInterface;

/**
 * Plugin implementation of the 'text_default' formatter.
 */
#[FieldFormatter(
  id: 'list_key',
  label: new TranslatableMarkup('Key'),
  field_types: ['string', 'decimal', 'numeric', 'integer', 'float'],
)]
class KeyFormatter implements DataFieldFormatterInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements($item, $langcode) {
    if (is_null($item->value)) {
      return $item->value;
    }
    return [
      '#type' => 'processed_text',
      '#text' => property_exists($item, 'value_original') ? $item->value_original : $item->value,
      '#langcode' => $langcode,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    return [];
  }

}
