<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataField\FieldType\DateItem;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Plugin implementation of the 'datetime timestamp' widget.
 */
#[FieldWidget(
  id: 'timestamp',
  label: new TranslatableMarkup("Datetime Timestamp"),
  field_types: ['date', 'datetime_iso8601'],
)]
class TimestampDatetimeWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    if (in_array($setting["type"], ['datetime_iso8601', 'date'])) {
      $element["#type"] = 'date';
      $element["#attributes"]['type'] = 'datetime-local';
      $element['#process_default_value'] = FALSE;
      if (!empty($element["#default_value"])) {
        $element["#default_value"] = date('Y-m-d H:i:s', is_numeric($element["#default_value"]) ? $element["#default_value"] : strtotime($element["#default_value"]));
      }
      return $element;
    }

    $element['#title'] = '';
    if (!empty($element["#field_settings"]["min"])) {
      $element["#min"] = $element["#field_settings"]["min"];
    }
    if (!empty($element["#field_settings"]["max"])) {
      $element["#max"] = $element["#field_settings"]["max"];
    }
    $element['#type'] = 'datetime';
    $element['#date_time_format'] = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
    if (empty($element["#default_value"]) && !empty($element["#field_settings"]["default_date_type"])) {
      $relativeDate = DateItem::DEFAULT_VALUE_NOW;
      if ($element["#field_settings"]["default_date_type"] == DateItem::DEFAULT_VALUE_CUSTOM) {
        $relativeDate = $element["#field_settings"]["default_date"];
      }
      if (!empty($relativeDate)) {
        $date = new DrupalDateTime($relativeDate);
        $element["#default_value"] = $date->format($element['#date_time_format']);
      }
    }
    if (!empty($element["#default_value"])) {
      if (is_numeric($element["#default_value"])) {
        $date = \date($element['#date_time_format'], $element["#default_value"]);
        $element['#default_value'] = new DrupalDateTime($date);
      }
      if (is_string($element["#default_value"])) {
        $element['#default_value'] = new DrupalDateTime($element['#default_value']);
      }
    }
    if (is_object($element['#default_value'])) {
      $element['#default_value']->setTimezone(new \DateTimezone(date_default_timezone_get()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['size'] = 30;
    $options['placeholder'] = t('Please type');
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'size' => [
        '#type' => 'number',
        '#title' => $this->t('Size'),
        '#default_value' => $widget_settings['size'] ?? self::defaultSettings()['size'],
        '#min' => 1,
      ],
      'placeholder' => [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder'),
        '#default_value' => $widget_settings['placeholder'] ?? self::defaultSettings()['placeholder'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues($date, array $form, FormStateInterface $form_state) {
    if (empty($date)) {
      return $date;
    }
    if (is_string($date)) {
      $date = strtotime($date);
    }
    if (is_numeric($date)) {
      $date = DrupalDateTime::createFromTimestamp($date);
    }
    if ($form['#storage_settings']['type'] == 'date') {
      $storage_format = 'Y-m-d H:i:s';
      return $date->format($storage_format);
    }
    return $date->getTimestamp();
  }

}
