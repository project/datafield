<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'Language' widget.
 */
#[FieldWidget(
  id: 'radios',
  label: new TranslatableMarkup('Radio buttons'),
  field_types: ['boolean', 'string', 'integer', 'numeric', 'float'],
)]
class RadiosWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $label = empty($setting['required']) ? $this->t('N/A') : $this->t('- None -');
    $element['#empty_option'] = $label;
    if (!empty($element["#field_settings"]["on_label"])) {
      $element["#field_settings"]["list"] = TRUE;
      $element["#field_settings"]['allowed_values'] = [
        $element["#field_settings"]["off_label"],
        $element["#field_settings"]["on_label"],
      ];
    }
    if (!empty($element["#field_settings"]["list"])) {
      if (is_string($element["#field_settings"]['allowed_values'])) {
        $element["#field_settings"]['allowed_values'] = static::extractAllowedValues($element["#field_settings"]['allowed_values']);
      }
      $element['#options'] = $element["#field_settings"]['allowed_values'];
    }
    else {
      $element['#type'] = 'checkbox';
    }
  }

  /**
   * Extracts the allowed values array from the allowed_values element.
   *
   * @param string $string
   *   The raw string to extract values from.
   *
   * @return array
   *   The array of extracted key/value pairs.
   *
   * @see \Drupal\options\Plugin\Field\FieldType\ListTextItem::extractAllowedValues()
   */
  protected static function extractAllowedValues(string $string): array {
    return array_reduce(
      array_filter(array_map('trim', explode(PHP_EOL, $string))),
      function ($values, $text) {
        if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
          $key = trim($matches[1]);
          $value = trim($matches[2]);
        }
        else {
          $key = $value = $text;
        }
        $values[$key] = $value;
        return $values;
      },
      []
    );
  }

}
