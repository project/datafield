<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'text_textarea' widget.
 */
#[FieldWidget(
  id: 'textarea',
  label: new TranslatableMarkup('Text area (multiple rows)'),
  field_types: ['text', 'blob', 'json'],
)]
class TextareaWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    if (!empty($element["#widget_settings"]['rows'])) {
      $element['#rows'] = $element["#widget_settings"]['rows'];
    }
    if (!empty($element["#widget_settings"]['placeholder'])) {
      $element['#placeholder'] = $element["#widget_settings"]['placeholder'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['placeholder'] = t('Please type');
    $options['cols'] = 10;
    $options['rows'] = 5;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'placeholder' => [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder'),
        '#default_value' => $widget_settings['placeholder'] ?? self::defaultSettings()['placeholder'],
      ],
      'cols' => [
        '#type' => 'number',
        '#title' => $this->t('Columns'),
        '#default_value' => $widget_settings['cols'] ?? self::defaultSettings()['cols'],
        '#min' => 1,
        '#description' => $this->t('How many columns wide the textarea should be'),
      ],
      'rows' => [
        '#type' => 'number',
        '#title' => $this->t('Rows'),
        '#default_value' => $widget_settings['rows'] ?? self::defaultSettings()['rows'],
        '#min' => 1,
        '#description' => $this->t('How many rows high the textarea should be.'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $settings += self::defaultSettings();
    $summary[] = $this->t('Cols: @cols', ['@cols' => $settings['cols']]);
    $summary[] = $this->t('Rows: @rows', ['@rows' => $settings['rows']]);
    return $summary;
  }

}
