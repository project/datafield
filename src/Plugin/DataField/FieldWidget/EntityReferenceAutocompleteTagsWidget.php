<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\views\Views;

/**
 * Plugin implementation of the 'entity_reference_autocomplete_tags' widget.
 */
#[FieldWidget(
  id: 'entity_reference_autocomplete_tags',
  label: new TranslatableMarkup('Autocomplete (Modal style)'),
  field_types: ['entity_reference'],
)]
class EntityReferenceAutocompleteTagsWidget extends EntityReferenceAutocompleteWidget {

  use StringTranslationTrait;

  /**
   * Get form element.
   *
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item, $setting) {
    $element = parent::getFormElement($element, $item, $setting);
    $element['#attributes']['class'][] = 'mt-0';
    $element['#wrapper_attributes']['class'][] = 'container-inline d-flex flex-inline';
    $fieldSettings = $element["#field_settings"];
    $selection_settings = [
      'match_operator' => 'CONTAINS',
      'match_limit' => 10,
    ];
    $checkView = explode(':', $fieldSettings['target_bundles']);
    if (empty($checkView[1])) {
      $fieldSettings["handler"] = 'default:' . $fieldSettings['entity_reference_type'];
      $selection_settings['target_bundles'] = [$fieldSettings['target_bundles'] => $fieldSettings['target_bundles']];
    }
    else {
      $fieldSettings["handler"] = 'views';
      $selection_settings['view'] = [
        'view_name' => $checkView[0],
        'display_name' => $checkView[1],
        'arguments' => [],
      ];
    }
    $data = serialize($selection_settings) . $fieldSettings['entity_reference_type'] . $fieldSettings["handler"];
    $selection_settings_key = Crypt::hmacBase64($data, Settings::getHashSalt());
    $key_value_storage = $this->keyValue->get('entity_autocomplete');
    if (!$key_value_storage->has($selection_settings_key)) {
      $key_value_storage->set($selection_settings_key, $selection_settings);
    }
    $url = Url::fromRoute('datafield.field_reference', [
      'target_type' => $fieldSettings['entity_reference_type'],
      'selection_handler' => $fieldSettings["handler"],
      'selection_settings_key' => $selection_settings_key,
    ])->toString();
    $field_name = $element["#field_name"];
    $element['#description'] = '<span class="search-reference-widget" role="button" data-reference="' . $field_name . '" data-url="' . $url . '" >🔎</span>';
    $element['#attached']['library'][] = 'datafield/search-reference-widget';
    $element['#attached']['drupalSettings']['datafield'][$field_name] = $this->fieldReferenceSettings($fieldSettings['target_bundles']);
    return $element;
  }

  /**
   * Set drupal settings for search entity.
   *
   * @param string $target_bundle
   *   The bundle select or views.
   *
   * @return array
   *   The settings columns.
   */
  protected function fieldReferenceSettings(string $target_bundle): array {
    $settings = [
      'columns' => [
        ['field' => 'state', 'radio' => "true"],
      ],
    ];
    $searchKey = 'name';
    $checkView = explode(':', $target_bundle);
    if (!empty($checkView[1])) {
      $view = Views::getView($checkView[0]);
      $view->setDisplay($checkView[1]);
      $displayHandler = $view->displayHandlers->get($checkView[1]);
      $styleOptions = $displayHandler->getPlugin('style')->options['search_fields'];
      $styleOptions = array_filter($styleOptions);
      $searchKey = array_key_first($styleOptions);
      $listType = [
        'list_default',
        'options_select',
        'list_key',
        'options_buttons',
        'list_key',
      ];
      $settings['options'] = $displayHandler->getPlugin('row')->options;
      $fields = $displayHandler->getOption('fields');
      foreach ($fields as $fieldId => $field) {
        $type = $field["type"];
        $typeSearch = in_array($type, $listType) ? 'select' : 'input';
        $settings['columns'][] = [
          'field' => $fieldId,
          'title' => !empty($field['label']) ? $field['label'] : $field["entity_field"] ?? '',
          'sortable' => "true",
          'filter-control' => $typeSearch,
        ];
      }
    }
    else {
      $settings['columns'][] = [
        'field' => $searchKey,
        'title' => $this->t('Name'),
        'sortable' => "true",
      ];
    }
    $settings['search_key'] = $searchKey;
    return $settings;
  }

}
