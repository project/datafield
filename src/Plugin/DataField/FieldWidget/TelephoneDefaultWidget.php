<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'telephone_default' widget.
 */
#[FieldWidget(
  id: 'telephone',
  label: new TranslatableMarkup('Telephone number'),
  field_types: ['string', 'telephone'],
)]
class TelephoneDefaultWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $element['#type'] = 'tel';
    $element['#attributes']['autocorrect'] = 'on';
    $element['#attributes']['mozactionhint'] = 'go';
    if (!empty($element["#field_settings"]["list"])) {
      $idDatalist = $element['#field_name'] . '-' . $element['#machine_name'];
      $element['#attributes']['list'] = $idDatalist;
      $element['#attributes']['pattern'] = "[0-9+\-\s]+";
      $element['#attributes']['title'] = $this->t("Please enter a valid phone number");
      $element['#attributes']['class'][] = 'datalistfield';
      $element['#attached']['drupalSettings']['datalistfield'][$idDatalist] = $element["#field_settings"]["allowed_values"];
      $element['#attached']['library'][] = 'datafield/datalistfield';
    }
    else {
      $element['#attached']['library'][] = 'datafield/formatnumber';
      $element['#attributes']['onfocus'] = 'use_telphone(this)';
      $element['#attributes']['separator'] = $setting["separator"] ?? ' ';
      $element['#attributes']['step'] = $setting["step"] ?? 2;
    }
    if (!empty($element["#widget_settings"]["size"])) {
      $element["#size"] = $element["#widget_settings"]["size"];
    }
    if (!empty($setting["placeholder"])) {
      $element["#placeholder"] = $setting["placeholder"];
    }
    $element['#maxlength'] = $setting['max_length'];

  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    $path = explode('.', $violation->getPropertyPath());
    $subfield = end($path);
    if (!empty($element[$subfield])) {
      $settings = $element[$subfield]['#field_settings'];
      if (!empty($settings['list'])) {
        return FALSE;
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['size'] = 30;
    $options['placeholder'] = t('Please type');
    $options['separator'] = ' ';
    $options['step'] = 2;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'size' => [
        '#type' => 'number',
        '#title' => $this->t('Size'),
        '#default_value' => $widget_settings['size'] ?? self::defaultSettings()['size'],
        '#min' => 1,
      ],
      'placeholder' => [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder'),
        '#default_value' => $widget_settings['placeholder'] ?? self::defaultSettings()['placeholder'],
      ],
      'separator' => [
        '#type' => 'textfield',
        '#title' => $this->t('Separator'),
        '#default_value' => $widget_settings['separator'] ?? self::defaultSettings()['separator'],
        '#description' => $this->t('Specify a character to automatically separate large numbers.'),
        '#attributes' => [
          'maxlength' => 1,
          'size' => 1,
        ],
      ],
      'step' => [
        '#type' => 'number',
        '#title' => $this->t('step'),
        '#default_value' => $widget_settings['step'] ?? self::defaultSettings()['step'],
        '#min' => 0,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues($value, array $form, FormStateInterface $form_state) {
    $settings = $form['#widget_settings'];
    $value = str_replace($settings['separator'], '', $value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $settings += self::defaultSettings();
    $summary[] = $this->t('Size: @size', ['@size' => $settings['size']]);
    return $summary;
  }

}
