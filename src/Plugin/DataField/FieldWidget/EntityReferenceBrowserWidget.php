<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_reference_autocomplete_tags' widget.
 */
#[FieldWidget(
  id: 'entity_reference_browser',
  label: new TranslatableMarkup('Entity reference browser'),
  description: new TranslatableMarkup('An autocomplete text field with tagging.'),
  field_types: ['entity_reference'],
)]
class EntityReferenceBrowserWidget implements DataFieldWidgetInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a EntityReferenceAutocompleteWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected EntityTypeManagerInterface $entityTypeManager) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['entity_browser'] = '';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'entity_browser' => [
        '#type' => 'textfield',
        '#title' => $this->t('Entity browser id'),
        '#default_value' => $widget_settings['entity_browser'] ?? self::defaultSettings()['entity_browser'],
      ],
    ];
  }

  /**
   * Get form element.
   *
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item, $setting) {
    $id = $element["#id"];
    $element['#type'] = 'entity_browser';
    $element['#description'] = [
      '#type' => 'hidden',
      '#default_value' => $element['#default_value'],
      '#id' => $id,
    ];
    $element += [
      '#entity_browser' => $element["#widget_settings"]["entity_browser"],
      '#selection_mode' => 'selection_append',
      '#custom_hidden_id' => $id,
      '#cardinality' => 1,
      '#entity_browser_validators' => [
        'entity_type' => [
          'type' => $element["#entity_type"],
        ],
      ],
      '#process' => [
        [
          '\Drupal\entity_browser\Element\EntityBrowserElement',
          'processEntityBrowser',
        ],
        [
          'Drupal\entity_browser\Plugin\Field\FieldWidget\EntityReferenceBrowserWidget',
          'processEntityBrowser',
        ],
      ],
      '#attached' => [
        'drupalSettings' => [
          'entity_browser' => [
            $id => ['selector' => '#' . $id],
          ],
        ],
      ],
    ];
    if (isset($element["#default_value"]) && is_numeric($element["#default_value"])) {
      $entity = $this->entityTypeManager->getStorage($element["#entity_type"])
        ->load($element["#default_value"]);
      $element["#default_value"] = $entity;
    }
  }

}
