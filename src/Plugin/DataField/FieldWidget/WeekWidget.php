<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataField\FieldType\DateItem;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'datetime_default' widget.
 */
#[FieldWidget(
  id: 'week',
  label: new TranslatableMarkup("Week"),
  field_types: ['date', 'datetime_iso8601'],
)]
class WeekWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    if (!empty($element["#field_settings"]["min"])) {
      $element["#min"] = $element["#field_settings"]["min"];
    }
    if (!empty($element["#field_settings"]["max"])) {
      $element["#max"] = $element["#field_settings"]["max"];
    }
    $element['#type'] = 'date';
    $element['#date_timezone'] = date_default_timezone_get();
    $element['#attributes']['type'] = 'week';
    $format = 'Y-\WW';
    if (empty($element["#default_value"]) && !empty($element["#field_settings"]["default_date_type"])) {
      $relativeDate = DateItem::DEFAULT_VALUE_NOW;
      if ($element["#field_settings"]["default_date_type"] == DateItem::DEFAULT_VALUE_CUSTOM) {
        $relativeDate = $element["#field_settings"]["default_date"];
      }
      if (!empty($relativeDate)) {
        $date = new DrupalDateTime($relativeDate, date_default_timezone_get());
        $element["#default_value"] = $date->format($format);
      }
    }
    elseif (!empty($element["#default_value"])) {
      if (is_string($element["#field_storage"]["datetime_type"])) {
        $element["#field_storage"]["datetime_type"] = strtotime($element["#field_storage"]["datetime_type"]);
      }
      $date = new DrupalDateTime($element["#default_value"]);
      $element["#default_value"] = $date->format($format);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues($date, array $form, FormStateInterface $form_state) {
    if (empty($date)) {
      return $date;
    }
    $storage_format = is_object($date) ? 'Y-m-d' : 'Y-W';
    $datetime_type = $form["#storage_settings"]["datetime_type"];
    if ($datetime_type == 'week') {
      $storage_format = 'Y-\WW';
    }
    if (in_array($datetime_type, ['date', 'datetime'])) {
      $storage_format = 'Y-m-d';
    }
    if (is_string($date)) {
      $date = strtotime($date);
    }
    if (is_array($date)) {
      $date = current($date);
    }
    if (is_numeric($date)) {
      $date = DrupalDateTime::createFromTimestamp($date);
    }
    $value = $date->format($storage_format);
    return $value;
  }

}
