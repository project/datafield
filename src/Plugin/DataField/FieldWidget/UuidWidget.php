<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'datetime_default' widget.
 */
#[FieldWidget(
  id: 'uuid',
  label: new TranslatableMarkup('Universally Unique IDentifier'),
  field_types: ['uuid'],
)]
class UuidWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    if (!empty($setting["min"])) {
      $element["#min"] = $setting["min"];
    }
    if (!empty($setting["max"])) {
      $element["#max"] = $setting["max"];
    }
  }

}
