<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataField\FieldType\DateItem;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Drupal\datafield\Plugin\Field\FieldType\DataFieldItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Plugin implementation of the 'datetime_default' widget.
 */
#[FieldWidget(
  id: 'date',
  label: new TranslatableMarkup("Date"),
  field_types: ['date', 'datetime_iso8601'],
)]
class DateWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    if (!empty($element["#field_settings"]["min"])) {
      $element["#min"] = $element["#field_settings"]["min"];
    }
    if (!empty($element["#field_settings"]["max"])) {
      $element["#max"] = $element["#field_settings"]["max"];
    }
    if (!empty($setting["placeholder"])) {
      $element["#placeholder"] = $setting["placeholder"];
    }
    if (empty($element["#default_value"]) && !empty($element["#field_settings"]["default_date_type"])) {
      $relativeDate = DateItem::DEFAULT_VALUE_NOW;
      if ($element["#field_settings"]["default_date_type"] == DateItem::DEFAULT_VALUE_CUSTOM) {
        $relativeDate = $element["#field_settings"]["default_date"];
      }
      if (!empty($relativeDate)) {
        $date = new DrupalDateTime($relativeDate, date_default_timezone_get());
        $format = DateTimeItemInterface::DATE_STORAGE_FORMAT;
        $element["#default_value"] = $date->format($format);
      }
    }
    $element['#date_timezone'] = date_default_timezone_get();

    if ($setting['datetime_type'] == 'date') {
      $element['#date_time_element'] = 'none';
      $element['#date_time_format'] = '';
    }
    elseif (is_object($element['#default_value'])) {
      $element['#default_value']->setTimezone(new \DateTimezone(date_default_timezone_get()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['size'] = 30;
    $options['placeholder'] = t('Please type');
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'size' => [
        '#type' => 'number',
        '#title' => $this->t('Size'),
        '#default_value' => $widget_settings['size'] ?? self::defaultSettings()['size'],
        '#min' => 1,
      ],
      'placeholder' => [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder'),
        '#default_value' => $widget_settings['placeholder'] ?? self::defaultSettings()['placeholder'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues($date, array $form, FormStateInterface $form_state) {
    if (empty($date)) {
      return $date;
    }
    if (is_string($date)) {
      $date = strtotime($date);
    }
    if (is_numeric($date)) {
      $date = DrupalDateTime::createFromTimestamp($date);
    }
    $storage_format = DataFieldItem::DATETIME_DATE_STORAGE_FORMAT;
    return $date->format($storage_format);
  }

}
