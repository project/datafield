<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataField\FieldType\DateItem;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'datetime_default' widget.
 */
#[FieldWidget(
  id: 'time',
  label: new TranslatableMarkup("Time"),
  field_types: ['date', 'datetime_iso8601'],
)]
class TimeWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $element['#title'] = '';
    if (!empty($element["#field_settings"]["min"])) {
      $element["#min"] = $element["#field_settings"]["min"];
    }
    if (!empty($element["#field_settings"]["max"])) {
      $element["#max"] = $element["#field_settings"]["max"];
    }

    if ($setting['type'] == 'datetime_iso8601') {
      $element['#type'] = 'date';
      $element['#attributes']['type'] = 'time';
    }
    else {
      $element['#type'] = 'datetime';
      $element['#date_date_element'] = 'none';
      $element['#date_time_element'] = 'time';
      $element['#date_time_format'] = 'H:i';
    }
    if (empty($element["#default_value"]) && !empty($element["#field_settings"]["default_date_type"])) {
      $relativeDate = DateItem::DEFAULT_VALUE_NOW;
      if ($element["#field_settings"]["default_date_type"] == DateItem::DEFAULT_VALUE_CUSTOM) {
        $relativeDate = $element["#field_settings"]["default_date"];
      }
      if (!empty($relativeDate)) {
        $date = new DrupalDateTime($relativeDate, date_default_timezone_get());
        $format = 'H:i';
        $element["#default_value"] = $date->format($format);
      }
    }
    if (!empty($element["#default_value"]) && is_string($element["#default_value"])) {
      if ($element["#field_storage"]["type"] != 'datetime_iso8601') {
        $element['#default_value'] = DrupalDateTime::createFromFormat('H:i:s', $element['#default_value']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues($date, array $form, FormStateInterface $form_state) {
    if (empty($date)) {
      return $date;
    }
    $storage_format = 'H:i:s';
    if (is_string($date)) {
      $date = strtotime($date);
    }
    if (is_numeric($date)) {
      $date = DrupalDateTime::createFromTimestamp($date);
    }
    return $date->format($storage_format);
  }

}
