<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataField\FieldType\DateItem;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Drupal\datafield\Plugin\Field\FieldType\DataFieldItem;

/**
 * Plugin implementation of the 'datetime_default' widget.
 */
#[FieldWidget(
  id: 'datetime',
  label: new TranslatableMarkup("Date and time"),
  field_types: ['date', 'datetime_iso8601'],
)]
class DateTimeDefaultWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    if (in_array($setting["type"], ['datetime_iso8601', 'date'])) {
      $element["#type"] = 'date';
      $element["#attributes"]['type'] = 'datetime-local';
      $element['#process_default_value'] = FALSE;
      if (!empty($element["#default_value"])) {
        $element["#default_value"] = date('Y-m-d H:i', strtotime($element["#default_value"]));
      }
      return $element;
    }
    $element['#title'] = '';
    $element["#type"] = 'datelist';
    if (!empty($element["#field_settings"]["min"])) {
      $element["#min"] = $element["#field_settings"]["min"];
    }
    if (!empty($element["#field_settings"]["max"])) {
      $element["#max"] = $element["#field_settings"]["max"];
    }
    $element['#date_time_format'] = 'H:i';
    $format = 'Y-m-d H:i:s';
    if (empty($element["#default_value"]) && !empty($element["#field_settings"]["default_date_type"])) {
      $relativeDate = DateItem::DEFAULT_VALUE_NOW;
      if ($element["#field_settings"]["default_date_type"] == DateItem::DEFAULT_VALUE_CUSTOM) {
        $relativeDate = $element["#field_settings"]["default_date"];
      }
      if (!empty($relativeDate)) {
        $date = new DrupalDateTime($relativeDate);
        $element["#default_value"] = $date->format($format);
      }
    }
    if (!empty($element["#default_value"]) && is_string($element["#default_value"])) {
      $explode = explode('.', str_replace('T', ' ', $element["#default_value"]));
      $element['#default_value'] = current($explode);
      $element['#default_value'] = date($format, strtotime($element['#default_value']));
      $element['#default_value'] = DrupalDateTime::createFromFormat($format, $element['#default_value']);
      $element['#process_default_value'] = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues($date, array $form, FormStateInterface $form_state) {
    if (empty($date) || $form["#storage_settings"]["type"] == 'datetime_iso8601') {
      return $date;
    }
    if (is_string($date)) {
      $date = strtotime($date);
    }
    if (is_numeric($date)) {
      $date = DrupalDateTime::createFromTimestamp($date);
    }
    $storage_format = DataFieldItem::DATETIME_DATETIME_STORAGE_FORMAT;
    return $date->format($storage_format);
  }

}
