<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'string_textfield' widget.
 */
#[FieldWidget(
  id: 'textfield',
  label: new TranslatableMarkup('Textfield'),
  field_types: ['string', 'email'],
)]
class StringTextfieldWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    if (!empty($element["#field_settings"]["list"])) {
      $idDatalist = $element['#field_name'] . '-' . $element['#machine_name'];
      $element['#type'] = 'textfield';
      $element['#attributes']['list'] = $idDatalist;
      $element['#attributes']['class'][] = 'datalistfield';
      $element['#attributes']['autocomplete'] = 'off';
      $element['#attached']['drupalSettings']['datalistfield'][$idDatalist] = $element["#field_settings"]["allowed_values"];
      $element['#attached']['library'][] = 'datafield/datalistfield';
    }
    $element['#maxlength'] = $setting['max_length'];
    $element['#attributes']['data-bs-toggle'] = 'tooltip';
    $element['#attributes']['data-bs-title'] = $element['#default_value'] ?? '';
    $element['#attributes']['title'] = $element['#default_value'] ?? '';
    if ($element['#type'] == 'range') {
      $element['#description'] = $element['#default_value'] . ' ';
      $element['#attached']['library'][] = 'datafield/tooltip';
    }
    $settings = $element["#widget_settings"];
    if (!empty($settings['class'])) {
      $element['value']['#attributes']['class'][] = $settings['class'];
      unset($settings['class']);
    }
    foreach ($settings as $attribute => $val) {
      if (!empty($val) && in_array($attribute, ['size', 'step', 'maxlength', 'placeholder'])) {
        $element['#' . $attribute] = $val;
      }
      elseif (!empty($val)) {
        $element['#attributes'][$attribute] = $val;
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'type' => 'textfield',
      'placeholder' => t('Please type'),
      'separator' => '',
      'maxlength' => '',
      'pattern' => '',
      'class' => '',
      'size' => 30,
      'step' => '',
      'list' => '',
      'min' => '',
      'max' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'type' => [
        '#type' => 'select',
        '#title' => $this->t('Type for input'),
        '#options' => [
          'textfield' => $this->t('Text'),
          'number' => $this->t('Number'),
          'range' => $this->t('Range number'),
          'search' => $this->t('Search'),
          'tel' => $this->t('Telephone'),
          'email' => $this->t('Email'),
          'url' => $this->t('Url'),
          'week' => $this->t('Week'),
          'month' => $this->t('Month'),
          'time' => $this->t('Time'),
          'date' => $this->t('Date'),
          'datetime-local' => $this->t('Datetime local'),
        ],
        '#default_value' => $widget_settings['type'],
      ],
      'maxlength' => [
        '#type' => 'number',
        '#title' => $this->t('Max length'),
        '#default_value' => $widget_settings['maxlength'] ?? self::defaultSettings()['maxlength'],
        '#min' => 1,
      ],
      'size' => [
        '#type' => 'number',
        '#title' => $this->t('Size'),
        '#default_value' => $widget_settings['size'] ?? self::defaultSettings()['size'],
        '#min' => 1,
      ],
      'placeholder' => [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder'),
        '#default_value' => $widget_settings['placeholder'] ?? self::defaultSettings()['placeholder'],
      ],
      'pattern' => [
        '#type' => 'textfield',
        '#title' => $this->t('Regex pattern for input'),
        '#default_value' => $widget_settings['pattern'] ?? self::defaultSettings()['pattern'],
      ],
      'step' => [
        '#type' => 'textfield',
        '#title' => $this->t('Step'),
        '#default_value' => $widget_settings['step'] ?? self::defaultSettings()['step'],
      ],
      'min' => [
        '#type' => 'number',
        '#title' => $this->t('Min'),
        '#default_value' => $widget_settings['min'] ?? self::defaultSettings()['min'],
      ],
      'max' => [
        '#type' => 'number',
        '#title' => $this->t('Max'),
        '#default_value' => $widget_settings['max'] ?? self::defaultSettings()['max'],
      ],
      'list' => [
        '#type' => 'textfield',
        '#title' => $this->t('List for datalist'),
        '#default_value' => $widget_settings['list'] ?? self::defaultSettings()['list'],
      ],
      'class' => [
        '#type' => 'textfield',
        '#title' => $this->t('Custom class'),
        '#default_value' => $widget_settings['class'] ?? self::defaultSettings()['class'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $settings += self::defaultSettings();
    $summary[] = $this->t('Size: @size', ['@size' => $settings['size']]);
    return $summary;
  }

}
