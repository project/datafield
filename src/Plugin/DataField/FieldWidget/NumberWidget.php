<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'number' widget.
 */
#[FieldWidget(
  id: 'number',
  label: new TranslatableMarkup('Number field'),
  field_types: ['integer', 'numeric', 'float'],
)]
class NumberWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $element['#step'] = 1;
    if (!empty($element["#field_settings"]["min"])) {
      $element['#min'] = $element["#field_settings"]["min"];
    }
    if (!empty($element["#field_settings"]["max"])) {
      $element['#max'] = $element["#field_settings"]["max"];
    }
    if (!empty($element["#field_settings"]["prefix"])) {
      $element['#field_prefix'] = $element["#field_settings"]["prefix"];
    }
    if (!empty($element["#field_settings"]["suffix"])) {
      $element['#field_suffix'] = $element["#field_settings"]["suffix"];
    }
    if ($setting["type"] == 'numeric') {
      $element['#step'] = pow(0.1, $setting['scale'] ?? 2);
      if (!empty($element["#field_settings"]["step"])) {
        $element['#step'] = $element["#field_settings"]["step"];
      }
      if (empty($setting['scale']) || empty($element["#field_settings"]["step"])) {
        $element['#step'] = 'any';
      }
    }
    elseif ($setting["type"] == 'float') {
      $element['#step'] = 'any';
    }
    $element['#attributes']['class'][] = 'text-end text-align-right';
    if (!empty($element["#field_settings"]["list"])) {
      $idDatalist = $element['#field_name'] . '-' . $element['#machine_name'];
      $element['#attributes']['list'] = $idDatalist;
      $element['#attributes']['class'][] = 'datalistfield';
      $element['#attached']['drupalSettings']['datalistfield'][$idDatalist] = $element["#field_settings"]["allowed_values"];
      $element['#attached']['library'][] = 'datafield/datalistfield';
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['size'] = 30;
    $options['placeholder'] = t('Please type');
    $options['step'] = 2;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'size' => [
        '#type' => 'number',
        '#title' => $this->t('Size'),
        '#default_value' => $widget_settings['size'] ?? self::defaultSettings()['size'],
        '#min' => 1,
      ],
      'placeholder' => [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder'),
        '#default_value' => $widget_settings['placeholder'] ?? self::defaultSettings()['placeholder'],
      ],
      'step' => [
        '#type' => 'number',
        '#title' => $this->t('Step'),
        '#default_value' => $widget_settings['step'] ?? self::defaultSettings()['step'],
      ],
    ];
  }

}
