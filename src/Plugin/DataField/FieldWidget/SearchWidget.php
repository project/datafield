<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'string_textfield' widget.
 */
#[FieldWidget(
  id: 'search',
  label: new TranslatableMarkup('Search - autocomplete'),
  field_types: ['string'],
)]
class SearchWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $element['#type'] = 'search';
    if (!empty($setting['autocomplete'])) {
      $element['#autocomplete_route_name'] = 'datafield.json';
      $element['#autocomplete_query_parameters'] = [
        'machine_name' => $element["#machine_name"],
        'delta' => $element["#delta"],
        'limit' => $setting['limit'] ?? 10,
        'fill' => $setting['fill'] ?? FALSE,
      ];
      $element['#autocomplete_route_parameters'] = [
        'entity_type' => $element["#entity_type"],
        'bundle' => $element["#bundle"],
        'field_name' => $element["#field_name"],
      ];
      if (!empty($setting['fill'])) {
        $element['#attributes']['class'][] = 'datafield-autocomplete';
      }
    }
    if (!empty($element['#attributes']['placeholder'])) {
      $element['#attributes']['aria-label'] = $element['#attributes']['placeholder'];
    }
    $element['#attributes']['autocorrect'] = 'on';
    $element['#attributes']['mozactionhint'] = 'go';
    if (!empty($element["#field_settings"]["list"])) {
      $idDatalist = $element['#field_name'] . '-' . $element['#machine_name'];
      $element['#attributes']['list'] = $idDatalist;
      $element['#attributes']['class'][] = 'datalistfield';
      $element['#attached']['drupalSettings']['datalistfield'][$idDatalist] = $element["#field_settings"]["allowed_values"];
      $element['#attached']['library'][] = 'datafield/datalistfield';
    }
    $element['#attached']['library'][] = 'datafield/autocomplete';
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    $path = explode('.', $violation->getPropertyPath());
    $subfield = end($path);
    if (!empty($element[$subfield])) {
      $settings = $element[$subfield]['#field_settings'];
      if (!empty($settings['list'])) {
        return FALSE;
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['autocomplete'] = FALSE;
    $options['fill'] = FALSE;
    $options['limit'] = 10;
    $options['size'] = 30;
    $options['placeholder'] = t('Search');
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    $field_name = $form["#field_name"];
    return [
      'autocomplete' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Autocomplete field'),
        '#description' => $this->t('It will fill auto data if it exists'),
        '#default_value' => $widget_settings['autocomplete'] ?? self::defaultSettings()['autocomplete'],
      ],
      'limit' => [
        '#type' => 'number',
        '#title' => $this->t('Limit'),
        '#description' => $this->t('The number of result lines returned'),
        '#default_value' => $widget_settings['limit'] ?? self::defaultSettings()['limit'],
        '#states' => [
          'invisible' => [":input[name='fields[$field_name][settings_edit_form][settings][widget_settings][text][autocomplete]']" => ['checked' => FALSE]],
        ],
      ],
      'fill' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Fill in all fields'),
        '#description' => $this->t('It will automatically populate all the selected data'),
        '#default_value' => $widget_settings['fill'] ?? self::defaultSettings()['fill'],
        '#states' => [
          'invisible' => [":input[name='fields[$field_name][settings_edit_form][settings][widget_settings][text][autocomplete]']" => ['checked' => FALSE]],
        ],
      ],
      'size' => [
        '#type' => 'number',
        '#title' => $this->t('Size'),
        '#default_value' => $widget_settings['size'] ?? self::defaultSettings()['size'],
        '#min' => 1,
      ],
      'placeholder' => [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder'),
        '#default_value' => $widget_settings['placeholder'] ?? self::defaultSettings()['placeholder'],
      ],
    ];
  }

}
