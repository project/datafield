<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the color_field spectrum widget.
 */
#[FieldWidget(
  id: 'color',
  label: new TranslatableMarkup('Color HTML5'),
  field_types: ['color', 'string'],
)]
class ColorWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $element['#type'] = 'color';
    if (!empty($element["#field_settings"]["list"])) {
      $idDatalist = $element['#field_name'] . '-' . $element['#machine_name'];
      $element['#attributes']['list'] = $idDatalist;
      $element['#attributes']['class'][] = 'datalistfield';
      $element['#attached']['drupalSettings']['datalistfield'][$idDatalist] = $element["#field_settings"]["allowed_values"];
      $element['#attached']['library'][] = 'datafield/datalistfield';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    $path = explode('.', $violation->getPropertyPath());
    $subfield = end($path);
    if (!empty($element[$subfield])) {
      $settings = $element[$subfield]['#field_settings'];
      if (!empty($settings['list'])) {
        return FALSE;
      }
    }
    return $element;
  }

}
