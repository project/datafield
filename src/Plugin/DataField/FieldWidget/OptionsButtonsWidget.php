<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'options_buttons' widget.
 */
#[FieldWidget(
  id: 'options_buttons',
  label: new TranslatableMarkup('Check boxes/radio buttons'),
  field_types: ['entity_reference'],
)]
class OptionsButtonsWidget implements DataFieldWidgetInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a OptionsSelectWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selectionManager
   *   The selection plugin manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected SelectionPluginManagerInterface $selectionManager, protected readonly ModuleHandlerInterface $moduleHandler) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('plugin.manager.entity_reference_selection'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $element['#type'] = 'radios';
    $optionsView = $element['#selection_settings']['options_view'] ?? [];
    $options = $this->getOptions($optionsView);
    $element['#options'] = $options;
  }

  /**
   * Returns the array of options for the widget.
   *
   * {@inheritdoc}
   */
  protected function getOptions($selection_options) {
    $handler = $this->selectionManager->getInstance($selection_options);
    $options = $handler->getReferenceableEntities();
    if (!empty($options)) {
      $options = reset($options);
    }
    $entity = $selection_options['entity'];
    $context = [
      'entity' => $entity,
      'options' => $selection_options,
    ];
    $this->moduleHandler->alter('options_list', $options, $context);
    array_walk_recursive($options, [$this, 'sanitizeLabel']);
    return $options;
  }

  /**
   * Sanitizes a string label to display as an option.
   *
   * @param string $label
   *   The label to sanitize.
   */
  protected function sanitizeLabel(&$label) {
    // Allow a limited set of HTML tags.
    $label = FieldFilteredMarkup::create($label);
  }

}
