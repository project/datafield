<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'media_library_widget' widget.
 *
 * @internal
 *   Plugin classes are internal.
 */
#[FieldWidget(
  id: 'media_library',
  label: new TranslatableMarkup('Media library'),
  description: new TranslatableMarkup('Allows you to select items from the media library.'),
  field_types: ['file'],
)]
class MediaLibraryWidget implements DataFieldWidgetInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a OptionsSelectWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected ModuleHandlerInterface $moduleHandler) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $element = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'media',
      "#default_value" => $element['#default_value'],
      '#allowed_bundles' => [
        'audio', 'document', 'image',
        'video', 'remote_video',
      ],
    ];
    if ($this->moduleHandler->moduleExists('media_library_form_element')) {
      $element['#type'] = 'media_library';
    }

  }

}
