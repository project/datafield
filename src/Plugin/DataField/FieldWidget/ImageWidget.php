<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'image_image' widget.
 */
#[FieldWidget(
  id: 'image_image',
  label: new TranslatableMarkup('Image'),
  field_types: ['image', 'file', 'entity_reference'],
)]
class ImageWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['preview_image_style'] = 'thumbnail';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $fieldName = str_replace('field_', '', $element['#field_name']);
    if (empty($element["#field_settings"]["file_directory"])) {
      $element["#field_settings"]["file_directory"] = $fieldName;
    }
    if (empty($element["#field_settings"]["uri_scheme"])) {
      $element["#field_settings"]["uri_scheme"] = 'public';
    }
    $element['#upload_location'] = self::doGetUploadLocation($element["#field_settings"], $setting);
    $element['#process_default_value'] = FALSE;
    if (!empty($element["#default_value"])) {
      if (is_object($element["#default_value"])) {
        $element["#default_value"] = [$element["#default_value"]->id()];
      }
      if (is_numeric($element["#default_value"])) {
        $element["#default_value"] = [$element["#default_value"]];
      }
    }

    $element["#type"] = 'managed_file';
    $element["#upload_validators"] = [
      'file_validate_extensions' => ['gif png jpg jpeg'],
    ];
    $element['#theme'] = 'image_widget';
    $element['#preview_image_style'] = self::defaultSettings()['preview_image_style'];
    if (!empty($setting["preview_image_style"])) {
      $element['#preview_image_style'] = $setting["preview_image_style"];
    }
  }

  /**
   * Determines the URI for a file field.
   *
   * @param array $settings
   *   The array of field settings.
   * @param array $data
   *   An array of token objects to pass to Token::replace().
   *
   * @return string
   *   An unsanitized file directory URI with tokens replaced. The result of
   *   the token replacement is then converted to plain text and returned.
   *
   * @see \Drupal\Core\Utility\Token::replace()
   */
  protected static function doGetUploadLocation(array $settings, $data = []) {
    $destination = trim($settings['file_directory'], '/');

    // Replace tokens. As the tokens might contain HTML we convert it to plain
    // text.
    $destination = PlainTextOutput::renderFromHtml(\Drupal::token()->replace($destination, $data));
    return $settings['uri_scheme'] . '://' . $destination;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'preview_image_style' => [
        '#title' => $this->t('Preview image style'),
        '#type' => 'select',
        '#options' => image_style_options(FALSE),
        '#empty_option' => '<' . $this->t('no preview') . '>',
        '#default_value' => $widget_settings['preview_image_style'] ?? self::defaultSettings()['preview_image_style'],
        '#description' => $this->t('The preview image will be shown while editing the content.'),
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $settings += self::defaultSettings();
    $summary[] = $this->t('Image style: @preview', ['@preview' => $settings['preview_image_style']]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues($value, array $form, FormStateInterface $form_state) {
    $entityType = $form["#field_settings"]["entity_reference_type"] ?? 'image';
    $checkEntityFile = in_array($entityType, ['file', 'image']);
    if ($checkEntityFile && is_array($value)) {
      $fid = current($value);
      // Set file to permanent .
      $file = File::load($fid);
      $status = $file->get('status')->value;
      if (!$status) {
        $file->set('status', TRUE)->save();
      }
      return $fid;
    }
    return $value;
  }

}
