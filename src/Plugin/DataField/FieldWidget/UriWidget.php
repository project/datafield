<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'uri' widget.
 */
#[FieldWidget(
  id: 'url',
  label: new TranslatableMarkup('URL'),
  field_types: ['uri', 'string'],
)]
class UriWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    if (!empty($setting["min"])) {
      $element["#min"] = $setting["min"];
    }
    if (!empty($setting["max"])) {
      $element["#max"] = $setting["max"];
    }
    $element['#maxlength'] = 2048;
    $element['#attributes']['autocorrect'] = 'on';
    $element['#attributes']['mozactionhint'] = 'go';
    if (!empty($element["#field_settings"]["list"])) {
      $idDatalist = $element['#field_name'] . '-' . $element['#machine_name'];
      $element['#attributes']['list'] = $idDatalist;
      $element['#attributes']['class'][] = 'datalistfield';
      $element['#attached']['drupalSettings']['datalistfield'][$idDatalist] = $element["#field_settings"]["allowed_values"];
      $element['#attached']['library'][] = 'datafield/datalistfield';
    }
    $element['#description'] = $this->t('This must be an external URL such as %url.', [
      '%url' => 'http://example.com',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    $path = explode('.', $violation->getPropertyPath());
    $subfield = end($path);
    if (!empty($element[$subfield])) {
      $settings = $element[$subfield]['#field_settings'];
      if (!empty($settings['list'])) {
        return FALSE;
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['size'] = 30;
    $options['placeholder'] = t('Please type');
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'size' => [
        '#type' => 'number',
        '#title' => $this->t('Size'),
        '#default_value' => $widget_settings['size'] ?? self::defaultSettings()['size'],
        '#min' => 1,
      ],
      'placeholder' => [
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder'),
        '#default_value' => $widget_settings['placeholder'] ?? self::defaultSettings()['placeholder'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $settings += self::defaultSettings();
    $summary[] = $this->t('Size: @size', ['@size' => $settings['size'] ?? self::defaultSettings()['size']]);
    return $summary;
  }

}
