<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'Language' widget.
 */
#[FieldWidget(
  id: 'select',
  label: new TranslatableMarkup('Select'),
  field_types: ['string', 'integer', 'numeric', 'float'],
)]
class SelectWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    self::setOptions($element, $setting);
  }

  /**
   * {@inheritdoc}
   */
  public static function setOptions(&$element, $setting) {
    $element['#empty_option'] = t('- Select a value -');
    if (!empty($element["#field_settings"]['list']) && !empty($element["#field_settings"]['allowed_values'])) {
      if (is_string($element["#field_settings"]['allowed_values'])) {
        $element["#field_settings"]['allowed_values'] = static::extractAllowedValues($element["#field_settings"]['allowed_values']);
      }
      $element['#options'] = $element["#field_settings"]['allowed_values'];
    }
  }

  /**
   * Extracts the allowed values array from the allowed_values element.
   *
   * @param string $string
   *   The raw string to extract values from.
   *
   * @return array
   *   The array of extracted key/value pairs.
   *
   * @see \Drupal\options\Plugin\Field\FieldType\ListTextItem::extractAllowedValues()
   */
  protected static function extractAllowedValues(string $string): array {
    return array_reduce(
      array_filter(array_map('trim', explode(PHP_EOL, $string))),
      function ($values, $text) {
        if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
          $key = trim($matches[1]);
          $value = trim($matches[2]);
        }
        else {
          $key = $value = $text;
        }
        $values[$key] = $value;
        return $values;
      },
      []
    );
  }

}
