<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'boolean_checkbox' widget.
 */
#[FieldWidget(
  id: 'checkbox',
  label: new TranslatableMarkup('Single on/off checkbox'),
  field_types: ['boolean'],
)]
class BooleanCheckboxWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $title = $element["#widget_settings"]["label"] ?? '';
    $element['#title'] = $title;
    if (isset($element['#title_display'])) {
      unset($element['#title_display']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['label'] = t('Ok');
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $widget_settings['label'] ?? self::defaultSettings()['label'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $settings += self::defaultSettings();
    $summary[] = $this->t('Label: @label', ['@label' => $settings['label']]);
    return $summary;
  }

}
