<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'number format' widget.
 */
#[FieldWidget(
  id: 'number_format',
  label: new TranslatableMarkup('Number format'),
  field_types: ['integer', 'numeric', 'float'],
)]
class NumberFormatWidget extends NumberWidget implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a EntityReferenceAutocompleteWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected AccountInterface $currentUser,) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    parent::getFormElement($element, $item, $setting);
    if ($setting['type'] == 'integer') {
      $limit = [
        // Signed.
        [
          'big' => ['min' => -9223372036854775808, 'max' => 9223372036854775807],
          'normal' => ['min' => -2147483648, 'max' => 2147483647],
          'medium' => ['min' => -8388608, 'max' => 8388607],
          'small' => ['min' => -32768, 'max' => 32767],
          'tiny' => ['min' => -128, 'max' => 127],
        ],
        // Unsigned.
        [
          'big' => ['min' => 0, 'max' => 18446744073709551615],
          'normal' => ['min' => 0, 'max' => 4294967295],
          'medium' => ['min' => 0, 'max' => 16777215],
          'small' => ['min' => 0, 'max' => 65535],
          'tiny' => ['min' => 0, 'max' => 255],
        ],
      ];
      $unsigned = $setting['unsigned'] ?? 0;
      $size = $setting['size'] ?? 0;
      if (isset($limit[$unsigned][$size])) {
        $element['#attributes']['min'] = $limit[$unsigned][$size]['min'];
        $element['#attributes']['max'] = $limit[$unsigned][$size]['max'];
      }
      $element['#attributes']['class'][] = 'text-end text-align-right';
    }
    if (in_array($setting['type'], ['decimal', 'float', 'numeric'])) {
      $element['#maxlength'] = $setting["precision"] + $setting["scale"] + 1;
    }
    $element['#type'] = 'textfield';
    $element['#attributes']['class'][] = 'text-format-number';
    $element['#attached']['library'][] = 'datafield/auto_numeric';
    $element['#attached']['library'][] = 'datafield/formatnumber';
    $languageId = $this->currentUser->getPreferredLangcode();
    $language = 'British';
    $formatNum = [
      'French' => ['fr', 'af', 'ar', 'be', 'bg', 'br', 'cs', 'eo', 'et', 'fi',
        'ht', 'hu', 'hy', 'ka', 'kk', 'ky', 'lt', 'lv', 'mg', 'nb', 'nn', 'oc',
        'os', 'pl', 'prs', 'ps', 'pt', 'pt-pt', 'ru', 'rw', 'se', 'sk', 'sq',
        'sv', 'tyv', 'uk',
      ],
      'Spanish' => ['de', 'ast', 'az', 'bs', 'ca', 'da', 'el', 'es', 'eu', 'fo',
        'fy', 'gl', 'hr', 'id', 'is', 'it', 'jv', 'ku', 'mk', 'nl', 'pt-br',
        'ro', 'sl', 'sr', 'tr', 'vi',
      ],
    ];
    foreach ($formatNum as $lang => $languageIso) {
      if (in_array($languageId, $languageIso)) {
        $language = $lang;
        break;
      }
    }
    $element['#attached']['drupalSettings']['auto_numeric']['language'] = $language;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues($value, array $form, FormStateInterface $form_state) {
    return preg_replace('/[^\d.-]/', '', $value);
  }

}
