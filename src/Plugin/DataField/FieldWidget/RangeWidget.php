<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'range' widget.
 */
#[FieldWidget(
  id: 'range',
  label: new TranslatableMarkup('Range fields'),
  field_types: ['integer', 'numeric', 'float'],
)]
class RangeWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options['step'] = 1;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $widget_settings = $form['#settings'];
    return [
      'step' => [
        '#type' => 'number',
        '#title' => $this->t('Step'),
        '#default_value' => $widget_settings['step'] ?? self::defaultSettings()['step'],
        '#step' => 'any',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings = []) {
    $summary = [];
    $settings += self::defaultSettings();
    $summary[] = $this->t('Step: @step', ['@step' => $settings['step']]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
    $element['#step'] = $setting["step"] ?? 1;
    $element['#min'] = $element["#field_settings"]["min"] ?? 0;
    $element['#max'] = $element["#field_settings"]["max"] ?? 100;
    $element['#attributes']['data-bs-toggle'] = 'tooltip';
    $element['#attributes']['data-bs-title'] = $element['#default_value'] ?? '';
    $element['#attributes']['title'] = $element['#default_value'] ?? '';
    $element['#description'] = $element['#default_value'] . ' ';
    $element['#attached']['library'][] = 'datafield/tooltip';
    if ($setting["type"] == 'numeric' && empty($setting["step"])) {
      if (!empty($setting['scale'])) {
        $setting['#step'] = pow(0.1, $setting['scale']);
      }
    }
    elseif ($setting["type"] == 'float') {
      $element['#step'] = 'any';
    }
    if (!empty($element["#field_settings"]["list"])) {
      $idDatalist = $element['#field_name'] . '-' . $element['#machine_name'];
      $element['#attributes']['list'] = $idDatalist;
      $element['#attributes']['class'][] = 'datalistfield';
      $element['#attached']['drupalSettings']['datalistfield'][$idDatalist] = $element["#field_settings"]["allowed_values"];
      $element['#attached']['library'][] = 'datafield/datalistfield';
    }
  }

}
