<?php

namespace Drupal\datafield\Plugin\DataField\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datafield\Plugin\DataFieldWidgetInterface;

/**
 * Plugin implementation of the 'string_textfield' widget.
 */
#[FieldWidget(
  id: 'hidden',
  label: new TranslatableMarkup('Hidden'),
  field_types: ['string', 'blob'],
)]
class HiddenWidget implements DataFieldWidgetInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormElement(&$element, $item = NULL, $setting = []) {
  }

}
