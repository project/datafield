<?php

namespace Drupal\datafield\Plugin\DataField\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datafield\Attribute\DataFieldType;
use Drupal\datafield\Plugin\DataFieldTypeInterface;

/**
 * Defines the 'json' field type.
 */
#[DataFieldType(
  id: 'json',
  label: new TranslatableMarkup('Json'),
  description: new TranslatableMarkup('This field stores a number in the database in a integer.'),
  default_widget: 'json',
  default_formatter: 'json',
  no_ui: TRUE,
)]
class JsonItem implements DataFieldTypeInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'type' => 'json',
      'pgsql_type' => 'json',
      'mysql_type' => 'json',
      'not null' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(array $settings) {
    $default = self::defaultStorageSettings();
    return [
      'columns' => [
        'value' => $default,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue() {
    $random = new Random();
    $values['value'] = $random->object();
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->t('Json');
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $field_settings, array $settings) {
    $field_form = [
      'label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $field_settings['label'] ?? ucfirst($settings["name"]),
      ],
      'required' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Required'),
        '#default_value' => $field_settings['required'] ?? FALSE,
      ],
    ];

    return $field_form;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    return [];
  }

  /**
   * Get constraints.
   *
   * {@inheritdoc}
   */
  public function getConstraints(array $settings) {
    $constraints = [];
    if ($settings['required']) {
      $constraints['NotBlank'] = [];
    }
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(array $settings) {
    $name = $settings['name'];
    $data_type = 'string';
    return DataDefinition::create($data_type)
      ->setLabel(new TranslatableMarkup('%name value', ['%name' => $name]))
      ->setRequired(FALSE);
  }

}
