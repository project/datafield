<?php

namespace Drupal\datafield\Plugin\DataField\FieldType;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datafield\Attribute\DataFieldType;
use Drupal\datafield\Plugin\DataFieldTypeInterface;

/**
 * Defines the 'serial' entity field type.
 */
#[DataFieldType(
  id: 'serial',
  label: new TranslatableMarkup('Serial'),
  description: new TranslatableMarkup('An entity field containing a serial.'),
  default_widget: 'textfield',
  default_formatter: 'string',
  no_ui: TRUE,
)]
class SerialItem implements DataFieldTypeInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'type' => 'serial',
      'size' => 'normal',
      'serialize' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(array $settings) {
    $default = self::defaultStorageSettings();
    if (!empty($settings['size'])) {
      $default['size'] = $settings['size'];
    }
    if (!empty($settings['unsigned'])) {
      $default['unsigned'] = $settings['unsigned'];
    }
    return [
      'columns' => [
        'value' => $default,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->t('Serial');
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue() {
    $min = 0;
    $max = 999;
    $values['value'] = mt_rand($min, $max);
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $field_settings, array $settings) {
    $field_form = [
      'label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $field_settings['label'] ?? '',
      ],
    ];
    return $field_form;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(array $settings) {
    $constraints = [];
    if ($settings['required']) {
      $constraints['NotBlank'] = [];
    }
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(array $settings) {
    $name = $settings['name'];
    $data_type = 'string';
    return DataDefinition::create($data_type)
      ->setLabel(new TranslatableMarkup('%name value', ['%name' => $name]))
      ->setRequired(FALSE);
  }

}
