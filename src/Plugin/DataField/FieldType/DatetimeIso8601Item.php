<?php

namespace Drupal\datafield\Plugin\DataField\FieldType;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datafield\Attribute\DataFieldType;
use Drupal\datafield\Plugin\DataFieldTypeInterface;
use Drupal\datafield\Plugin\Field\FieldType\DataFieldItem;

/**
 * Defines the 'timestamp' entity field type.
 */
#[DataFieldType(
  id: 'datetime_iso8601',
  label: new TranslatableMarkup('Date ISO'),
  description: new TranslatableMarkup('An entity field containing a UNIX timestamp value.'),
  default_widget: 'datetime',
  default_formatter: 'timestamp',
)]
class DatetimeIso8601Item implements DataFieldTypeInterface {
  use StringTranslationTrait;

  /**
   * Defines the default value as now.
   */
  const DEFAULT_VALUE_NOW = 'now';

  /**
   * Defines the default value as relative.
   */
  const DEFAULT_VALUE_CUSTOM = 'relative';

  /**
   * Defines max length stock string format date.
   */
  const MAX_LENGTH = 30;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'type' => 'varchar',
      'length' => self::MAX_LENGTH,
      'not null' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(array $settings) {
    $default = self::defaultStorageSettings();
    if ($settings['datetime_type'] == 'timestamp') {
      $default = [
        'type' => 'int',
        'length' => 10,
        'unsigned' => TRUE,
      ];
    }
    return [
      'columns' => [
        'value' => $default,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue($settings = []) {
    $date_type = $settings['datetime_type'];
    $timestamp = \Drupal::time()->getRequestTime() - mt_rand(0, 86400 * 365);
    $storage_format = $date_type == 'date' ? DataFieldItem::DATETIME_DATE_STORAGE_FORMAT : DataFieldItem::DATETIME_DATETIME_STORAGE_FORMAT;
    return gmdate($storage_format, $timestamp);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(array $settings) {
    $name = $settings['name'];
    $data_type = 'string';
    switch ($settings['datetime_type']) {
      case 'datetime':
        $data_type = 'datetime_iso8601';
        break;

      case 'timestamp':
        $data_type = 'timestamp';
        break;
    }
    return DataDefinition::create($data_type)
      ->setLabel(new TranslatableMarkup('%name value', ['%name' => $name]))
      ->setRequired(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->t('Date iso');
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $field_settings, array $settings) {
    $visible = ':input[id="edit-settings-field-settings-' .
      str_replace('_', '-', $field_settings["machine_name"]) .
      '-default-date-type"]';
    $field_form = [
      'label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $field_settings['label'] ?? ucfirst($settings["name"]),
      ],
      'required' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Required'),
        '#default_value' => $field_settings['required'] ?? FALSE,
      ],
      'min' => [
        '#type' => 'number',
        '#title' => $this->t('Minimum'),
        '#description' => $this->t('The minimum value that should be allowed in this field. Leave blank for no minimum.'),
        '#default_value' => $field_settings['min'] ?? NULL,
      ],
      'max' => [
        '#type' => 'number',
        '#title' => $this->t('Maximum'),
        '#description' => $this->t('The maximum value that should be allowed in this field. Leave blank for no maximum.'),
        '#default_value' => $field_settings['max'] ?? NULL,
      ],
      'default_date_type' => [
        '#type' => 'select',
        '#title' => $this->t('Default date'),
        '#description' => $this->t('Set a default value for this date.'),
        '#default_value' => $field_settings['default_date_type'] ?? '',
        '#options' => [
          static::DEFAULT_VALUE_NOW => $this->t('Current date'),
          static::DEFAULT_VALUE_CUSTOM => $this->t('Relative date'),
        ],
        '#empty_value' => '',
      ],
      'default_date' => [
        '#type' => 'textfield',
        '#title' => $this->t('Relative default value'),
        '#description' => $this->t("Describe a time by reference to the current day, like '+90 days' (90 days from the day the field is created) or '+1 Saturday' (the next Saturday). See <a href=\"http://php.net/manual/function.strtotime.php\">strtotime</a> for more details."),
        '#default_value' => (isset($field_settings['default_date_type']) && $field_settings['default_date_type'] == static::DEFAULT_VALUE_CUSTOM) ? $field_settings['default_date'] : '',
        '#states' => [
          'visible' => [
            $visible => ['value' => static::DEFAULT_VALUE_CUSTOM],
          ],
        ],
      ],
    ];
    if (in_array($settings["datetime_type"], ['date', 'datetime', 'timestamp'])) {
      $field_form['min']['#type'] = 'date';
      $field_form['max']['#type'] = 'date';
    }
    if (in_array($settings["datetime_type"], ['time'])) {
      $field_form['min']['#type'] = 'date';
      $field_form['min']['#attributes']['type'] = 'time';
      $field_form['max']['#type'] = 'date';
      $field_form['max']['#attributes']['type'] = 'time';
    }
    if (in_array($settings["datetime_type"], ['week'])) {
      $field_form['min']['#type'] = 'date';
      $field_form['min']['#attributes']['type'] = 'week';
      $field_form['max']['#type'] = 'date';
      $field_form['max']['#attributes']['type'] = 'week';
    }
    if (in_array($settings["datetime_type"], ['month'])) {
      $field_form['min']['#type'] = 'date';
      $field_form['min']['#attributes']['type'] = 'month';
      $field_form['max']['#type'] = 'date';
      $field_form['max']['#attributes']['type'] = 'month';
    }
    return $field_form;
  }

  /**
   * Get constraints.
   *
   * {@inheritdoc}
   */
  public function getConstraints(array $settings) {
    $constraints = [];
    if ($settings['required']) {
      $constraints['NotBlank'] = [];
    }
    return $constraints;
  }

}
