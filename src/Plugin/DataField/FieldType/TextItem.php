<?php

namespace Drupal\datafield\Plugin\DataField\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datafield\Attribute\DataFieldType;
use Drupal\datafield\Plugin\DataFieldTypeInterface;

/**
 * Defines the 'string_long' field type.
 */
#[DataFieldType(
  id: 'text',
  label: new TranslatableMarkup('Text (plain, long)'),
  description: new TranslatableMarkup('A field containing a long string value.'),
  category: 'text',
  default_widget: 'textarea',
  default_formatter: 'text_default',
)]
class TextItem implements DataFieldTypeInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'type' => 'text',
      'size' => 'big',
      'not null' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(array $settings) {
    $default = self::defaultStorageSettings();
    if (!empty($settings['size'])) {
      $default['size'] = $settings['size'];
    }
    return [
      'columns' => [
        'value' => $default,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue($settings = []) {
    $random = new Random();
    $values['value'] = $random->paragraphs();
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(array $settings) {
    $name = $settings['name'];
    $data_type = 'string';
    return DataDefinition::create($data_type)
      ->setLabel(new TranslatableMarkup('%name value', ['%name' => $name]))
      ->setRequired(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->t('Text');
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $field_settings, array $settings) {
    $field_form = [
      'label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $field_settings['label'] ?? ucfirst($settings["name"]),
      ],
      'required' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Required'),
        '#default_value' => $field_settings['required'] ?? FALSE,
      ],
    ];

    return $field_form;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(array $settings) {
    $constraints = [];
    if ($settings['required']) {
      $constraints['NotBlank'] = [];
    }
    return $constraints;
  }

}
