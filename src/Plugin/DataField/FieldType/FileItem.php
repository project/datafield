<?php

namespace Drupal\datafield\Plugin\DataField\FieldType;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datafield\Attribute\DataFieldType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'file' field type.
 *
 * @DataFieldType(
 *   id = "file",
 *   label = @Translation("File"),
 *   description = @Translation("A field file/image value."),
 *   category = @Translation("Text"),
 *   default_widget = "managed_file",
 *   default_formatter = "file_uri",
 * )
 */
#[DataFieldType(
  id: 'file',
  label: new TranslatableMarkup('File'),
  description: new TranslatableMarkup('A data field file/image value.'),
  category: 'text',
  default_widget: 'managed_file',
  default_formatter: 'file_uri',
)]
class FileItem extends IntegerItem implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Constructs a EntityReferenceAutocompleteWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param mixed $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, protected StreamWrapperManagerInterface $streamWrapperManager) {
    unset($plugin_id, $plugin_definition, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration,
      $container->get('stream_wrapper_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'unsigned' => FALSE,
      // Valid size property values include: 'tiny', 'small', 'medium', 'normal'
      // and 'big'.
      'size' => 'normal',
      'type' => 'int',
      'not null' => FALSE,
      'file_extensions' => 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp',
      'file_directory' => '',
      'uri_scheme' => 'public',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->t('File');
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $field_settings, array $settings) {
    $directoryDefault = str_replace('field_', '', $field_settings["field_name"]) . '-' . $settings["name"];
    $scheme_options = $this->streamWrapperManager->getNames(StreamWrapperInterface::WRITE_VISIBLE);
    $field_form = [
      'label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $field_settings['label'] ?? ucfirst($settings["name"]),
      ],
      'required' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Required'),
        '#default_value' => $field_settings['required'] ?? FALSE,
      ],
      'uri_scheme' => [
        '#type' => 'radios',
        '#title' => $this->t('Upload destination'),
        '#options' => $scheme_options,
        '#default_value' => $field_settings['uri_scheme'] ?? 'public',
      ],
      'file_directory' => [
        '#type' => 'textfield',
        '#title' => $this->t('File directory'),
        '#default_value' => $field_settings['file_directory'] ?? $directoryDefault,
        '#description' => $this->t('Optional subdirectory within the upload destination where files will be stored. Do not include preceding or trailing slashes.'),
        // '#element_validate' => [[static::class, 'validateDirectory']],
      ],
      'file_extensions' => [
        '#type' => 'textfield',
        '#title' => $this->t('Allowed file extensions'),
        '#default_value' => $field_settings['file_extensions'] ?? self::defaultStorageSettings()['file_extensions'],
        '#description' => $this->t("Separate extensions with a comma or space. Each extension can contain alphanumeric characters, '.', and '_', and should start and end with an alphanumeric character."),
        '#maxlength' => 256,
        '#required' => TRUE,
      ],
    ];
    return $field_form;
  }

  /**
   * Get constraints.
   *
   * {@inheritdoc}
   */
  public function getConstraints(array $settings) {
    $constraints = [];
    if ($settings['required']) {
      $constraints['NotBlank'] = [];
    }
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(array $settings) {
    $name = $settings['name'];
    $data_type = 'entity_datafield';
    return DataDefinition::create($data_type)
      ->setLabel(new TranslatableMarkup('%name value', ['%name' => $name]))
      ->setRequired(FALSE);
  }

}
