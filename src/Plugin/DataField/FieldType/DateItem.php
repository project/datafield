<?php

namespace Drupal\datafield\Plugin\DataField\FieldType;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datafield\Attribute\DataFieldType;
use Drupal\datafield\Plugin\DataFieldTypeInterface;

/**
 * Defines the 'timestamp' entity field type.
 */
#[DataFieldType(
  id: 'date',
  label: new TranslatableMarkup('Date'),
  description: new TranslatableMarkup('An entity field containing a UNIX timestamp value.'),
  default_widget: 'datetime',
  default_formatter: 'timestamp',
)]
class DateItem implements DataFieldTypeInterface {
  use StringTranslationTrait;

  /**
   * Defines the default value as now.
   */
  const DEFAULT_VALUE_NOW = 'now';

  /**
   * Defines the default value as relative.
   */
  const DEFAULT_VALUE_CUSTOM = 'relative';

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'type' => 'date',
      'mysql_type' => 'date',
      'pgsql_type' => 'date',
      'sqlite_type' => 'varchar',
      'sqlsrv_type' => 'date',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(array $settings) {
    $default = self::defaultStorageSettings();
    switch ($settings['datetime_type']) {
      case 'datetime':
        $default = [
          'type' => 'datetime',
          'mysql_type' => 'datetime',
          'pgsql_type' => 'timestamp without time zone',
          'sqlite_type' => 'varchar',
          'sqlsrv_type' => 'smalldatetime',
        ];
        break;

      case 'timestamp':
        $default = [
          'type' => 'datestamp',
          'mysql_type' => 'timestamp',
          'pgsql_type' => 'timestamp',
          'sqlite_type' => 'varchar',
          'sqlsrv_type' => 'rowversion',
        ];
        break;

      case 'time':
        $default = [
          'type' => 'time',
          'mysql_type' => 'time',
          'pgsql_type' => 'time',
          'sqlite_type' => 'varchar',
          'sqlsrv_type' => 'time',
        ];
        break;

      case 'year':
        $default = [
          'type' => 'year',
          'mysql_type' => 'year',
          'pgsql_type' => 'varchar',
          'sqlite_type' => 'smallint',
          'sqlsrv_type' => 'smallint',
        ];
        break;

      case 'month':
        $default = [
          'type' => 'varchar',
          'length' => 8,
        ];
        break;

      case 'week':
        $default = [
          'type' => 'varchar',
          'length' => 9,
        ];
        break;

    }
    return [
      'columns' => [
        'value' => $default,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue() {
    // Pick a random timestamp in the past year.
    $timestamp = \Drupal::time()->getRequestTime() - mt_rand(0, 86400 * 365);
    $values['value'] = $timestamp;
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(array $settings) {
    $name = $settings['name'];
    $data_type = 'string';
    switch ($settings['datetime_type']) {
      case 'timespan':
        $data_type = 'timespan';
        break;

      case 'year':
        $data_type = 'integer';
        break;
    }
    return DataDefinition::create($data_type)
      ->setLabel(new TranslatableMarkup('%name value', ['%name' => $name]))
      ->setRequired(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->t('Date');
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $field_settings, array $settings) {
    $type = 'date';
    if ($settings["datetime_type"] == 'year') {
      $type = 'number';
    }
    $visible = ':input[id="edit-settings-field-settings-' .
      str_replace('_', '-', $field_settings["machine_name"]) .
      '-default-date-type"]';
    $field_form = [
      'label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#default_value' => $field_settings['label'] ?? ucfirst($settings["name"]),
      ],
      'required' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Required'),
        '#default_value' => $field_settings['required'] ?? FALSE,
      ],
      'min' => [
        '#type' => $type,
        '#title' => $this->t('Minimum'),
        '#description' => $this->t('The minimum value that should be allowed in this field. Leave blank for no minimum.'),
        '#default_value' => $field_settings['min'] ?? NULL,
      ],
      'max' => [
        '#type' => $type,
        '#title' => $this->t('Maximum'),
        '#description' => $this->t('The maximum value that should be allowed in this field. Leave blank for no maximum.'),
        '#default_value' => $field_settings['max'] ?? NULL,
      ],
      'default_date_type' => [
        '#type' => 'select',
        '#title' => $this->t('Default date'),
        '#description' => $this->t('Set a default value for this date.'),
        '#default_value' => $field_settings['default_date_type'] ?? '',
        '#options' => [
          static::DEFAULT_VALUE_NOW => $this->t('Current date'),
          static::DEFAULT_VALUE_CUSTOM => $this->t('Relative date'),
        ],
        '#empty_value' => '',
      ],
      'default_date' => [
        '#type' => 'textfield',
        '#title' => $this->t('Relative default value'),
        '#description' => $this->t("Describe a time by reference to the current day, like '+90 days' (90 days from the day the field is created) or '+1 Saturday' (the next Saturday). See <a href=\"http://php.net/manual/function.strtotime.php\">strtotime</a> for more details."),
        '#default_value' => (isset($field_settings['default_date_type']) && $field_settings['default_date_type'] == static::DEFAULT_VALUE_CUSTOM) ? $field_settings['default_date'] : '',
        '#states' => [
          'visible' => [
            $visible => ['value' => static::DEFAULT_VALUE_CUSTOM],
          ],
        ],
      ],
    ];

    if (in_array($settings["datetime_type"], ['time'])) {
      $field_form['min']['#type'] = 'date';
      $field_form['min']['#attributes']['type'] = 'time';
      $field_form['max']['#type'] = 'date';
      $field_form['max']['#attributes']['type'] = 'time';
    }
    if (in_array($settings["datetime_type"], ['week'])) {
      $field_form['min']['#type'] = 'date';
      $field_form['min']['#attributes']['type'] = 'week';
      $field_form['max']['#type'] = 'date';
      $field_form['max']['#attributes']['type'] = 'week';
    }
    if (in_array($settings["datetime_type"], ['month'])) {
      $field_form['min']['#type'] = 'date';
      $field_form['min']['#attributes']['type'] = 'month';
      $field_form['max']['#type'] = 'date';
      $field_form['max']['#attributes']['type'] = 'month';
    }
    return $field_form;
  }

  /**
   * Get constraints.
   *
   * {@inheritDoc}
   */
  public function getConstraints(array $settings) {
    $constraints = [];
    if (!empty($settings['required'])) {
      $constraints['NotBlank'] = [];
    }
    return $constraints;
  }

}
