<?php

namespace Drupal\datafield\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin type manager for data field widgets.
 *
 * @ingroup field_widget
 */
class DataFieldWidgetPluginManager extends DefaultPluginManager {

  /**
   * The field type manager to define field.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * Constructs a ArchiverManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/DataField/FieldWidget',
      $namespaces,
      $module_handler,
      'Drupal\datafield\Plugin\DataFieldWidgetInterface',
      FieldWidget::class,
      'Drupal\Core\Field\Annotation\FieldWidget',
    );
    $this->alterInfo($this->getType());
    $this->setCacheBackend($cache_backend, 'data_field_widget_types_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function getType() {
    return 'data_field_widget_info';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginClass($type) {
    return $this->getDefinition($type)['class'];
  }

}
