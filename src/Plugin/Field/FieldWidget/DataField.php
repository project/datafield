<?php

namespace Drupal\datafield\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'data_field' widget.
 */
#[FieldWidget(
  id: 'data_field_widget',
  label: new TranslatableMarkup('Data Field'),
  field_types: ['data_field'],
)]
class DataField extends Base {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();
    $size = 12;
    $columns = range(1, $size);
    foreach ($elements["widget_settings"] as $subfield => &$widget_setting) {
      $widget_setting['col'] = [
        '#type' => 'select',
        '#title' => $this->t('Column width'),
        '#options' => array_combine($columns, $columns),
        '#empty_option' => $this->t('Auto-layout columns'),
        '#description' => $this->t('Bootstrap grid system'),
        '#default_value' => $settings['widget_settings'][$subfield]['col'] ?? '',
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_settings = $this->getFieldSettings();
    $storage = $this->getFormElement($items, $delta, $element, $form, $form_state);
    $settings = $this->getSettings();
    $widget = [
      '#theme_wrappers' => ['container', 'form_element'],
      '#attributes' => ['class' => [Html::getId('data_field-elements'), 'row']],
      '#attached' => [
        'library' => ['datafield/widget'],
      ],
      '#type' => 'fieldset',
      '#base_type' => 'data_field',
    ];
    if ($settings['inline']) {
      $widget['#attributes']['class'][] = Html::getId('data_field-widget-inline');
    }
    foreach ($storage[$delta] as $subfield => $item) {
      if ($subfield == 'remove_button') {
        continue;
      }
      $settingWidget = $settings["widget_settings"][$subfield];
      $widget[$subfield] = $item;
      $widget_type = $item['#type'];
      $label_display = $settingWidget['label_display'] ?? 'hidden';
      $widget[$subfield]["#wrapper_attributes"]["class"][] = !empty($settingWidget["col"]) ? 'col-' . $settingWidget["col"] : 'col';
      unset($widget[$subfield]["#title_display"]);
      $label = $field_settings["field_settings"][$subfield]['label'];
      if ($label_display != 'hidden' && parent::isLabelSupported($widget_type)) {
        $widget[$subfield]['#title'] = $label;
        if ($label_display == 'invisible') {
          $widget[$subfield]['#title_display'] = 'invisible';
        }
        elseif ($label_display == 'inline') {
          $widget[$subfield]['#label_attributes']['class'][] = 'col-3';
          $widget[$subfield]['#title_display'] = 'inline';
          $widget[$subfield]['#wrapper_attributes']['class'][] = 'row';
          $widget[$subfield]['#wrapper_attributes']['class'][] = 'container-inline';
        }
      }
    }

    return $element + $widget;
  }

}
