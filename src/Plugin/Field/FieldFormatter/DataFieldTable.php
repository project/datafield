<?php

namespace Drupal\datafield\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Drupal\datafield\Plugin\DataFieldFormatterPluginManager;
use Drupal\datafield\Plugin\DataFieldTypePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementations for 'table' formatter.
 */
#[FieldFormatter(
  id: 'data_field_table_formatter',
  label: new TranslatableMarkup('Table'),
  field_types: [
    'data_field',
  ],
)]
class DataFieldTable extends Base {

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\datafield\Plugin\DataFieldTypePluginManager $pluginType
   *   Plugin data type service.
   * @param \Drupal\datafield\Plugin\DataFieldFormatterPluginManager $pluginFormatter
   *   Plugin data formatter service.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme
   *   The theme service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, DataFieldTypePluginManager $pluginType, DataFieldFormatterPluginManager $pluginFormatter, protected ThemeManagerInterface $theme) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $pluginType, $pluginFormatter);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id, $plugin_definition, $configuration['field_definition'],
      $configuration['settings'], $configuration['label'],
      $configuration['view_mode'], $configuration['third_party_settings'],
      $container->get('plugin.manager.data_field.type'),
      $container->get('plugin.manager.data_field.formatter'),
      $container->get('theme.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $default = [
      'direction' => 'horizontal',
      'mode' => 'table',
      'number_column' => FALSE,
      'number_column_label' => '№',
      'caption' => '',
      'footer_text' => '',
      'formatter_settings' => [],
      'datatables_options' => [
        'bExpandable' => TRUE,
        'bInfo' => TRUE,
        'scrollX' => TRUE,
        'bStateSave' => FALSE,
        'paging' => TRUE,
        'ordering' => TRUE,
        'searching' => TRUE,
        'show-export' => TRUE,
        'bMultiFilter' => FALSE,
      ],
      'bootstrap_table_options' => [
        'sticky-header' => TRUE,
        'search' => TRUE,
        'show-search-clear-button' => TRUE,
        'show-refresh' => TRUE,
        'show-toggle' => TRUE,
        'show-fullscreen' => TRUE,
        'show-columns' => TRUE,
        'data-detail-view' => TRUE,
        'show-columns-toggle-all' => TRUE,
        'mobile-responsive' => TRUE,
        'show-print' => TRUE,
        'show-copy-rows' => TRUE,
        'show-export' => TRUE,
        'sortable' => TRUE,
        'click-to-select' => TRUE,
        'show-pagination-switch' => TRUE,
        'pagination' => TRUE,
        'cookie' => TRUE,
        'show-footer' => FALSE,
      ],
    ];
    return $default + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $settings = $this->getSettings();
    $field_settings = $this->getFieldSettings();
    $element = parent::settingsForm($form, $form_state);
    $element['number_column'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable row number column'),
      '#default_value' => $settings['number_column'],
      '#attributes' => ['class' => [Html::getId('js-data_field-number-column')]],
    ];
    $element['number_column_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number column label'),
      '#size' => 30,
      '#default_value' => $settings['number_column_label'],
      '#states' => [
        'visible' => ['.js-data-field-number-column' => ['checked' => TRUE]],
      ],
    ];
    $element['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Table mode'),
      '#options' => [
        'table' => $this->t('Table'),
        'datatables' => $this->t('Datatables'),
        'bootstrap-table' => $this->t('Bootstrap table'),
      ],
      '#default_value' => $this->getSetting('mode'),
    ];
    $element['datatables_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Datatables Options'),
      '#options' => [
        'bExpandable' => $this->t('Table expendable'),
        'bInfo' => $this->t('Table information'),
        "scrollX" => $this->t('Scroll - horizontal'),
        'bStateSave' => $this->t('State save'),
        "ordering" => $this->t('Sortable'),
        'paging' => $this->t('Pagination'),
        'searching' => $this->t('Search'),
        'show-export' => $this->t('Buttons export'),
        'bMultiFilter' => $this->t('Multi filters'),
      ],
      '#default_value' => $this->getSetting('datatables_options'),
      '#states' => [
        'visible' => ['select[name*="[settings_edit_form][settings][mode]"]' => ['value' => 'datatables']],
      ],
    ];
    $element['bootstrap_table_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Bootstrap table Options'),
      '#options' => [
        'sticky-header' => $this->t('Sticky header'),
        'show-export' => $this->t('Buttons export'),
        'show-print' => $this->t('Button print'),
        'show-copy-rows' => $this->t('Copy rows'),
        'show-multi-sort' => $this->t('Show multi sort'),
        'show-fullscreen' => $this->t('Show full screen'),
        'search' => $this->t('Search'),
        'show-refresh' => $this->t('Refresh'),
        'show-toggle' => $this->t('Toggle'),
        'show-columns' => $this->t('Select columns'),
        'mobile-responsive' => $this->t('Reponsive'),
        'sortable' => $this->t('Sortable'),
        'click-to-select' => $this->t('Selectable'),
        'show-pagination-switch' => $this->t('Pagination switch'),
        'pagination' => $this->t('Pagination'),
        'show-footer' => $this->t('Footer'),
      ],
      '#default_value' => $this->getSetting('bootstrap_table_options'),
      '#states' => [
        'visible' => ['select[name*="[settings_edit_form][settings][mode]"]' => ['value' => 'bootstrap-table']],
      ],
    ];
    $element['direction'] = [
      '#title' => $this->t('Direction'),
      '#type' => 'select',
      '#options' => [
        'horizontal' => $this->t('Horizontal'),
        'vertical' => $this->t('Vertical'),
      ],
      '#default_value' => $this->getSetting('direction'),
    ];
    $element['caption'] = [
      '#title' => $this->t('Caption'),
      '#description' => $this->t('Caption of table.') .
      $this->t('Variable available {{ entity_type }}, {{ entity_bundle }}, {{ entity_field }}, {{ entity_id }}'),
      '#type' => 'textarea',
      '#default_value' => $this->getSetting('caption'),
    ];
    $subfields = array_keys($field_settings['columns']);
    if (!empty($setting = $settings['formatter_settings'])) {
      uasort($setting, [
        'Drupal\Component\Utility\SortArray',
        'sortByWeightElement',
      ]);
      $subfields = array_keys($setting);
    }
    foreach ($subfields as $subfield) {
      $item = $field_settings['columns'][$subfield];
      $title = $field_settings["field_settings"][$subfield]["label"] ?? $item['name'];
      $element['formatter_settings'][$subfield]['column_label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Column label: %title', ['%title' => $title]),
        '#size' => 30,
        '#attributes' => ['placeholder' => $this->t('Column label')],
        '#default_value' => $setting[$subfield]['column_label'] ?? $title,
      ];
      if (!empty($field_settings['columns'][$subfield]) && in_array($field_settings['columns'][$subfield]['type'], [
        'numeric',
        'integer',
        'float',
      ])) {
        $element['formatter_settings'][$subfield]['sum_column'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Sum column'),
          '#default_value' => $setting[$subfield]['sum_column'] ?? 0,
        ];
      }
    }

    $element['footer_text'] = [
      '#title' => $this->t('Custom text at the footer'),
      '#description' => $this->t('Variable available {{ entity_type }}, {{ entity_bundle }}, {{ entity_field }}, {{ entity_id }}'),
      '#type' => 'textarea',
      '#default_value' => $this->getSetting('footer_text'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $settings = $this->getSettings();
    $field_settings = $this->getFieldSettings();
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Enable row number column: @number_column', ['@number_column' => $settings['number_column'] ? $this->t('yes') : $this->t('no')]);
    if ($settings['number_column']) {
      $summary[] = $this->t('Number column label: @number_column_label', ['@number_column_label' => $settings['number_column_label']]);
    }

    $subfields = array_keys($field_settings['columns']);
    foreach ($subfields as $subfield) {
      if (!empty($settings['formatter_settings'][$subfield])) {
        $summary[] = ucfirst($subfield) . ' ' .
          $this->t('column label: @column_label',
            ['@column_label' => $settings['formatter_settings'][$subfield]['column_label'] ?? '']
          );
      }
    }
    $summary['mode'] = "Mode: " . $this->getSetting('mode');
    $summary['direction'] = "Direction: " . $this->getSetting('direction');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $field_settings = $this->getFieldSettings();
    $settings = $this->getSettings();
    $components = $field_settings["columns"] + $settings;
    $currentTheme = $this->theme->getActiveTheme();
    $theme_name = $currentTheme->getName();
    $theme_base = $currentTheme->getExtension()->base_theme ?? NULL;
    if (is_array($theme_base)) {
      $theme_base = array_key_first($theme_base);
    }
    $bootstrapTheme = [
      'bootstrap5',
      'bootstrap5_admin',
      'bootstrap_barrio',
      'radix',
    ];
    $entity = $items->getEntity();
    $entityId = $entity->id();
    $field_definition = $items->getFieldDefinition();
    $field_name = $items->getName();
    $table = ['#type' => 'table'];

    // No other way to pass context to the theme.
    // @see datafield_theme_suggestions_table_alter()
    $id = HTML::getId('data_field');
    $table['#attributes']['data-field--field-name'] = $field_name;
    $table['#attributes']['class'][] = $id . '-table';
    if (!empty($settings['custom_class'])) {
      $table['#attributes']['class'][] = $settings['custom_class'];
    }

    $subfields = array_keys($field_settings['columns']);

    $context = [
      'entity_bundle' => $entity->bundle(),
      'entity_type' => $field_definition->getTargetEntityTypeId(),
      'entity_field' => $field_name,
      'entity_id' => $entityId,
    ];

    if (!empty($setting = $settings['formatter_settings'])) {
      uasort($setting, [
        'Drupal\Component\Utility\SortArray',
        'sortByWeightElement',
      ]);
      $subfields = array_keys($setting);
    }
    $header = [];
    if (!empty($settings['number_column'])) {
      $header = ['index_col' => $settings['number_column_label']];
    }
    $hasPermission = $this->checkPermissionOperation($entity, $field_name);
    foreach ($subfields as $subfield) {
      if (empty($settings['number_column_label'])) {
        $header = [];
        break;
      }
      if (!empty($setting[$subfield]["hidden"])) {
        continue;
      }
      $label = $setting[$subfield]['column_label'] ?? $field_settings["field_settings"][$subfield]["label"];
      $header[$subfield] = $label;
      if (!isset($components[$subfield])) {
        $components[$subfield]["type"] = '';
      }
      $align[$subfield] = in_array($components[$subfield]["type"],
        ['integer', 'numeric', 'float']) ? 'right' : '';
      if (!empty($components['formatter_settings'][$subfield])) {
        $components[$subfield] += $components['formatter_settings'][$subfield];
      }
      if (!empty($align[$subfield])) {
        $header[$subfield] = [
          'data' => $label,
          'align' => $align[$subfield],
          'dir' => "rtl",
        ];
      }
    }
    if (!empty($settings['caption'])) {
      $table['#caption'] = [
        '#type' => 'inline_template',
        '#template' => $settings['caption'],
        '#context' => $context,
      ];
      $table['#attributes']['class'][] = 'caption-top';
    }
    if (!empty($header)) {
      if (!empty($this->getSetting("line_operations") && $hasPermission)) {
        $header['operation'] = '';
      }
      if ($this->getSetting("direction") != 'vertical') {
        $table['#header'] = $header;
      }
      switch ($settings["mode"]) {
        case 'datatables':
          $datatable_options = $this->datatablesOption($header, $components, $langcode);
          $table['#attributes']['width'] = '100%';
          $table['#attributes']['class'][] = 'data-table hover order-column stripe';
          if (in_array($theme_name, $bootstrapTheme) || in_array($theme_base, $bootstrapTheme)) {
            $table['#attached']['library'][] = 'datafield/datafield_datatables_bootstrap';
          }
          else {
            $table['#attached']['library'][] = 'datafield/datafield_datatables_css';
          }
          $table['#attached']['library'][] = 'datafield/datafield_datatables';
          $table['#attached']['drupalSettings']['datatables'][$field_name] = $datatable_options;
          break;

        case 'bootstrap-table':
          $bootstrapTable_options = $this->bootstrapTableOption($header, $components, $langcode);
          foreach ($bootstrapTable_options as $dataTable => $valueData) {
            $table['#attributes']["data-$dataTable"] = $valueData;
          }
          $table['#attributes']["data-cookie-id-table"] = $field_name;
          $table_header = [];
          foreach ($header as $field_name => $field_label) {
            if (is_string($field_label)) {
              $table_header[$field_name] = [
                'data' => $field_label,
                'data-field' => $field_name,
                'data-sortable' => "true",
                'class' => $field_name,
              ];
            }
            elseif (is_array($field_label)) {
              $table_header[$field_name] = $field_label + [
                'data-field' => $field_name,
                'data-sortable' => "true",
                'class' => $field_name,
              ];
            }
          }

          if (!empty($this->getSetting("line_operations") && $hasPermission)) {
            $table_header['operation'] = '';
          }

          if ($this->getSetting("direction") == 'vertical') {
            $table_header = ['name', 'value'];
            if ($settings['number_column']) {
              $table_header = [
                $settings["number_column_label"],
                $this->t('Name'),
                $this->t('Value'),
              ];
            }
          }
          $table['#header'] = $table_header;
          if (!empty($settings['footer_text'])) {
            $table['#footer'] = [
              [
                'class' => ['footer'],
                'data' => [
                  [
                    'data' => [
                      '#type' => 'inline_template',
                      '#template' => $settings['footer_text'],
                      '#context' => $context,
                    ],
                    'colspan' => count($table_header),
                  ],
                ],
              ],
            ];
            $table['#attributes']["data-show-footer"] = 'true';
          }
          $table['#attached']['library'][] = 'datafield/datafield_bootstraptable';
          break;
      }
    }
    $verticalIndex = 1;
    $sum_column = [];
    foreach ($items as $delta => $item) {
      if ($this->getSetting("direction") == 'vertical') {
        foreach ($subfields as $subfield) {
          if (!empty($setting[$subfield]["hidden"])) {
            continue;
          }
          if (!empty($setting[$subfield]["sum_column"]) && is_numeric($item->{$subfield})) {
            if (empty($sum_column[$subfield])) {
              $sum_column[$subfield] = 0;
            }
            $sum_column[$subfield] += $this->convertSumValue($item->{$subfield}, $components[$subfield]);
          }
          $row = [];
          if ($settings['number_column']) {
            $row[]['#markup'] = $verticalIndex++;
          }
          $label = '';
          if (!empty($settings["formatter_settings"][$subfield]["show_label"])) {
            $label = $settings["formatter_settings"][$subfield]["column_label"] ?? $field_settings['field_settings'][$subfield]["label"];
          }
          $row[]['#markup'] = $label;
          $row[] = [
            '#theme' => 'data_field_subfield',
            '#settings' => $settings,
            '#subfield' => $item->{$subfield},
            '#index' => $subfield,
            '#field_name' => $field_name,
          ];
          $table[] = $row;
        }
      }
      else {
        $row = [];
        if ($settings['number_column']) {
          $row[]['#markup'] = $delta + 1;
        }
        foreach ($subfields as $subfield) {
          if (!empty($setting[$subfield]["hidden"])) {
            continue;
          }
          if (!empty($setting[$subfield]["sum_column"])) {
            if (empty($sum_column[$subfield])) {
              $sum_column[$subfield] = 0;
            }
            $sum_column[$subfield] += $this->convertSumValue($item->{$subfield}, $components[$subfield]);
          }
          if (!empty($settings["formatter_settings"][$subfield]) && !empty($settings["formatter_settings"][$subfield]['hidden'])) {
            $row[]['#markup'] = '';
          }
          else {
            $label = '';
            if (!empty($settings["formatter_settings"][$subfield]["show_label"])) {
              $label = $settings["formatter_settings"][$subfield]["column_label"] ?? $field_settings['field_settings'][$subfield]["label"];
            }
            $cell = [
              '#theme' => 'data_field_subfield',
              '#settings' => $settings,
              '#subfield' => $item->{$subfield},
              '#index' => $subfield,
              '#field_name' => $field_name,
              '#label' => $label,
            ];
            if (!empty($align[$subfield])) {
              $cell['#wrapper_attributes'] = ['align' => $align[$subfield]];
            }
            $row[] = $cell;
          }
        }
        $route_params = [
          'entity_type' => $field_definition->getTargetEntityTypeId(),
          'field_name' => $field_name,
          'entity' => $entityId,
          'delta' => $delta,
        ];
        if (!empty($this->getSetting("line_operations") && $hasPermission)) {
          $row[] = [
            '#type' => 'dropbutton',
            '#access' => !empty($this->getSetting("line_operations")) && $hasPermission,
            '#links' => [
              'edit' => [
                'title' => $this->t('Edit'),
                'url' => Url::fromRoute('datafield.edit_form', $route_params),
              ],
              'duplicate' => [
                'title' => $this->t('Duplicate'),
                'url' => Url::fromRoute('datafield.clone_form', $route_params),
              ],
              'delete' => [
                'title' => $this->t('Remove'),
                'url' => Url::fromRoute('datafield.delete_form', $route_params),
              ],
            ],
          ];
        }
        $table[$delta] = $row;
      }
    }

    if (!empty($header) && !empty($sum_column)) {
      $footer = [];
      foreach (array_keys($header) as $colName) {
        $align = 'right';
        if (!empty($sum_column[$colName])) {
          $sumFormatter = number_format($sum_column[$colName], $components[$colName]['scale'] ?? 0, $components[$colName]['decimal_separator'] ?? ',', $components[$colName]['thousand_separator'] ?? ' ');
          if ($components[$colName]["type"] == 'integer') {
            $sumFormatter = number_format($sum_column[$colName], FALSE, FALSE, $components[$colName]['thousand_separator'] ?? ' ');
          }
          $footer[$colName] = [
            'data' => [
              '#markup' => $sumFormatter,
            ],
            'data-total' => $sum_column[$colName],
            'align' => $align,
            'class' => 'total-' . $colName,
          ];
        }
        else {
          $footer[$colName] = '';
        }
      }
      $table['#footer'] = [$footer];
    }
    $element[0] = $table;
    if (!empty($this->getSetting('form_format_table'))) {
      if ($entityId && $hasPermission) {
        $dialog_width = '80%';
        $element[] = [
          '#type' => 'container',
          'add-button' => [
            '#type' => 'link',
            '#access' => ($this->getSetting("line_operations") ?? FALSE) && $hasPermission,
            '#title' => Markup::create('<i class="bi bi-plus" aria-hidden="true"></i> ' . $this->t('Add')),
            '#url' => Url::fromRoute('datafield.add_form', $params = [
              'entity_type' => $field_definition->getTargetEntityTypeId(),
              'field_name' => $field_name,
              'entity' => $entityId,
            ]),
            '#attributes' => [
              'class' => [
                'btn',
                'btn-success',
                'use-ajax',
              ],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => Json::encode(['width' => $dialog_width]),
            ],
          ],
          'edit-button' => [
            '#type' => 'link',
            '#access' => $this->getSetting("line_operations") ?? FALSE,
            '#title' => Markup::create('<i class="bi bi-pencil-square"></i> ' . $this->t('Edit')),
            '#url' => Url::fromRoute('datafield.edit_all_form', $params = [
              'entity_type' => $field_definition->getTargetEntityTypeId(),
              'field_name' => $field_name,
              'entity' => $entityId,
            ]),
            '#attributes' => [
              'class' => [
                'btn',
                'btn-info',
                'use-ajax',
              ],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => Json::encode(['width' => $dialog_width]),
            ],
          ],
        ];
      }
    }
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function convertSumValue($value, $storage) {
    if (is_array($value)) {
      if (isset($value['#value'])) {
        $value = $value['#value'];
      }
      else {
        $value = reset($value);
      }
    }
    if (!empty($storage["thousand_separator"])) {
      if (!isset($storage["decimal_separator"]) || (!empty($storage["decimal_separator"]) && $storage["thousand_separator"] != $storage["decimal_separator"])) {
        $value = str_replace($storage["thousand_separator"], '', (string) $value);
      }
    }
    if (in_array($storage["type"], ['numeric', 'float'])) {
      if (!empty($storage["decimal_separator"])) {
        $value = str_replace($storage["decimal_separator"], '.', (string) $value);
      }
      $value = (float) $value;
    }
    else {
      $value = (int) $value;
    }
    return $value;
  }

  /**
   * Support Bootstrap Table.
   */
  public function bootstrapTableOption($header, $components, $langcode = 'en') {
    $data_option = self::defaultSettings()['bootstrap_table_options'];
    foreach ($components["bootstrap_table_options"] as $option => $bootstrapOption) {
      $data_option[$option] = !empty($bootstrapOption) ? 'true' : 'false';
    }
    $data_option += [
      'toggle' => 'table',
      'minimum-count-columns' => "2",
      'page-list' => "[10, 25, 50, 100, all]",
    ];

    $languages = [
      'af' => 'af-ZA',
      'am' => 'am-ET',
      'ar' => 'ar-AE',
      'az' => 'az-Latn-AZ',
      'be' => 'be-BY',
      'bg' => 'bg-BG',
      'ca' => 'ca-ES',
      'cs' => 'cs-CZ',
      'cy' => 'cy-GB',
      'da' => 'da-DK',
      'de' => 'de-DE',
      'en' => 'en-US',
      'el' => 'el-GR',
      'eo' => 'eo-EO',
      'es' => 'es-ES',
      'et' => 'et-EE',
      'eu' => 'eu-EU',
      'fa' => 'fa-IR',
      'fi' => 'fi-fi',
      'fr' => 'fr-FR',
      'ga' => 'ga-IE',
      'gl' => 'gl-ES',
      'gu' => 'gu-IN',
      'he' => 'he-IL',
      'hi' => 'hi-IN',
      'hr' => 'hr-HR',
      'hu' => 'hu-HU',
      'hy' => 'hy-AM',
      'id' => 'id-ID',
      'is' => 'is-IS',
      'it' => 'it-CH',
      'ja' => 'ja-JP',
      'ka' => 'ka-GE',
      'kk' => 'kk-KZ',
      'km' => 'km-KH',
      'ko' => 'ko-KR',
      'ky' => 'ky-KG',
      'lo' => 'lo-LA',
      'lt' => 'lt-LT',
      'lv' => 'lv-LV',
      'mk' => 'mk-MK',
      'ml' => 'ml-IN',
      'mn' => 'mn-MN',
      'ne' => 'ne-NP',
      'nl' => 'nl-NL',
      'nb' => 'nb-NO',
      'nn' => 'nn-NO',
      'pa' => 'pa-IN',
      'pl' => 'pl-PL',
      'pt' => 'pt-PT',
      'ro' => 'ro-RO',
      'ru' => 'ru-RU',
      'si' => 'si-LK',
      'sk' => 'sk-SK',
      'sl' => 'sl-SI',
      'sq' => 'sq-AL',
      'sr' => 'sr-Latn-RS',
      'sv' => 'sv-SE',
      'sw' => 'sw-KE',
      'ta' => 'ta-IN',
      'te' => 'te-IN',
      'th' => 'th-TH',
      'tr' => 'tr-TR',
      'uk' => 'uk-UA',
      'ur' => 'ur-PK',
      'vi' => 'vn-VN',
      'fil' => 'fi-FI',
      'zh-hans' => 'zh-CN',
      'zh-hant' => 'zh-TW',
    ];
    if (!empty($languages[$langcode])) {
      $data_option['locale'] = $languages[$langcode];
    }
    return $data_option;
  }

  /**
   * Datatable Options.
   */
  public function datatablesOption($header, $components, $langcode = 'en') {
    $datatable_options = self::defaultSettings()['datatables_options'];
    foreach ($components["datatables_options"] as $option => $tableOption) {
      $datatable_options[$option] = !empty($tableOption);
    }
    $datatable_options += [
      'dom' => 'Bfrtip',
      'bMultiFilter_position' => "header",
    ];
    foreach ($header as $field_name => $field_label) {
      $datatable_options['aoColumnHeaders'][] = $field_label;
      $column_options = [
        'name' => $field_name,
        'data' => $field_name,
        'orderable' => TRUE,
        'type' => 'html',
      ];

      // Attempt to autodetect the type of field in order to handle sorting.
      if (!empty($components[$field_name]) &&
      in_array($components[$field_name]['type'], [
        'number_decimal', 'number_integer', 'number_float',
        'list_float', 'list_integer',
      ])) {
        $column_options['type'] = 'html-num';
      }
      if (!empty($components[$field_name]) &&
        in_array($components[$field_name]['type'],
          ['datetime', 'date', 'datestamp'])) {
        $column_options['type'] = 'date-fr';
      }
      $datatable_options['columns'][] = $column_options;
    }
    $datatable_options['language'] = [
      "decimal" => "",
      "emptyTable" => $this->t("No data available in table"),
      "info" => implode(' ', [
        $this->t("Showing"),
        " _START_ ",
        $this->t('to'),
        '_END_ ',
        $this->t('of'),
        ' _TOTAL_ ',
        $this->t('entries'),
      ]),
      "infoEmpty" => $this->t("Showing 0 to 0 of 0 entries"),
      "infoFiltered" => $this->t("(filtered from _MAX_ total entries)"),
      "infoPostFix" => "",
      "thousands" => ",",
      "lengthMenu" => implode(' ', [
        $this->t("Show"),
        " _MENU_ ",
        $this->t('entries'),
      ]),
      "loadingRecords" => $this->t("Loading..."),
      "processing" => "",
      "search" => $this->t("Search:"),
      "zeroRecords" => $this->t("No matching records found"),
      "paginate" => [
        "first" => $this->t("First"),
        "last" => $this->t("Last"),
        "next" => $this->t("Next"),
        "previous" => $this->t("Previous"),
      ],
      "aria" => [
        "orderable" => $this->t("Order by this column"),
        "orderableReverse" => $this->t("Reverse order this column"),
      ],
    ];

    return $datatable_options;
  }

}
