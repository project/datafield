<?php

namespace Drupal\datafield\Hook;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\filter\FilterPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hook implementations for Data Field.
 */
class DataFieldHook {
  use StringTranslationTrait;

  /**
   * Constructs a new DataFieldHook object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The message service.
   * @param \Drupal\filter\FilterPluginManager|null $filterPluginManager
   *   Filter plugin manager.
   */
  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
    protected ConfigFactoryInterface $configFactory,
    protected MessengerInterface $messenger,
    protected ?FilterPluginManager $filterPluginManager = NULL,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('plugin.manager.filter'),
    );
  }

  /**
   * Implements hook_help().
   */
  #[Hook('help')]
  public function help($route_name, RouteMatchInterface $route_match) {
    switch ($route_name) {
      case 'help.page.datafield':
        $text = file_get_contents(__DIR__ . '/../../README.md');
        if (!$this->moduleHandler->moduleExists('markdown')) {
          return '<pre>' . Html::escape($text) . '</pre>';
        }
        else {
          // Use the Markdown filter to render the README.
          $settings = $this->configFactory->get('markdown.settings')->getRawData();
          $config = ['settings' => $settings];
          $filter = $this->filterPluginManager->createInstance('markdown', $config);
          return $filter->process($text, 'en');
        }

      default:
        return FALSE;
    }
    return FALSE;
  }

  /**
   * Implements hook_theme_suggestions_HOOK_alter().
   */
  #[Hook('theme_suggestions_table_alter')]
  public function themeSuggestionsTableAlter(array &$suggestions, array $variables): void {
    $field_name = $variables['attributes']['data-field--field-name'] ?? NULL;
    if ($field_name) {
      $suggestions[] = 'table__data_field__' . $field_name;
    }
  }

  /**
   * Implements hook_theme_suggestions_HOOK_alter().
   */
  #[Hook('theme_suggestions_details_alter')]
  public function themeSuggestionsDetailsAlter(array &$suggestions, array $variables): void {
    $field_name = $variables['element']['#attributes']['data-field--field-name'] ?? NULL;
    if ($field_name) {
      $suggestions[] = 'details__data_field__' . $field_name;
    }
  }

  /**
   * Implements hook_theme_suggestions_HOOK_alter().
   */
  #[Hook('theme_suggestions_item_list_alter')]
  public function themeSuggestionsItemListAlter(array &$suggestions, array $variables): void {
    $field_name = $variables['context']['data_field']['field_name'] ?? NULL;
    if ($field_name) {
      $suggestions[] = 'item_list__data_field__' . $field_name;
    }
  }

  /**
   * Implements hook_theme_suggestions_HOOK().
   */
  #[Hook('theme_suggestions_data_field_item')]
  public function themeSuggestionsDataFieldItem(array $variables): array {
    return ['data_field_item__' . $variables['elements']['#field_name']];
  }

  /**
   * Implements hook_theme_suggestions_HOOK().
   */
  #[Hook('theme_suggestions_data_field_definition_list')]
  public function themeSuggestionsDataFieldDefinitionList(array $variables): array {
    return ['data_field_definition_list__' . $variables['elements']['#field_name']];
  }

}
