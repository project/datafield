<?php

namespace Drupal\datafield\TypedData;

use Drupal\Core\TypedData\DataDefinition;

/**
 * {@inheritDoc}
 */
class EntityDataDefinition extends DataDefinition {

  /**
   * {@inheritdoc}
   */
  public function getMainPropertyName() {
    return NULL;
  }

}
