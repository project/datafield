(function ($, Drupal, once) {
  Drupal.behaviors.shortcutTable = {
    attach: function (context, settings) {
      $(once('tooltip-event', '[data-bs-toggle="tooltip"]', context)).on('input change', function () {
        let val = $(this).val();
        $(this).attr('data-bs-title', val);
        $(this).attr('title', val);
        if ($(this).attr('type') === 'range') {
          let min =  $(this).attr('min');
          let max =  $(this).attr('max');
          let $description = $(this).next('.description');
          let newVal = Number(((val - min) * 100) / (max - min));
          if ($description.length) {
            $description.text(val).css({
              left: `calc(${newVal}% + (${8 - newVal * 0.15}px))`,
              position: 'absolute',
              'margin-top': '-12px',
              transform: 'translateX(-50%)'
            }).parent().css('position', 'relative');
          }
        }
      });
    },
  };
})(jQuery, Drupal, once);
