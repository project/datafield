(function ($, Drupal, once) {
  Drupal.behaviors.shortcutTable = {
    attach: function (context, settings) {
      $(once('data-field-table-widget', '.field--widget-data-field-table-widget table', context)).each(function () {
        const table = $(this);
        // Automatically click add more button when focused
        table.closest('.form-item').find("input[name$='add_more']").on('focus', function () {
          $(this).click();
        });

        // Function to update weight selects
        function updateWeightSelects() {
          let total = table.find("tr").length - 1;
          table.find("tr").each(function (index) {
            const row = $(this);
            const weightSelect = row.find("select[name*='[_weight]']");

            if (weightSelect.length) {
              weightSelect.val(index - total); // Set the selected value to the row index
            }
          });
        }
        function removeWeightSelects() {
          table.find("select[name*='[_weight]']").remove();
        }

        // Hide button add more
        function hideAddMore() {
          table.closest('.form-item').find("input[name$='add_more']").hide();
        }

        // Function to duplicate current row
        function duplicateRow(row, index) {
          const clone = row.clone();
          const regex = /\[(\d+)\](?=\[[^\]]+\]$)/;
          // Update the names of inputs and selects in the duplicated row
          clone.find("input, select, textarea").each(function () {
            const field = $(this);
            const name = field.attr("name");
            if (name) {
              let newName = name.replace(regex, `[${index}]`);
              field.attr("name", newName);
            }
          });
          clone.insertAfter(row);
          removeWeightSelects();
          hideAddMore();
        }

        // Function to add a new row
        function addNewRow(index) {
          const lastRow = table.find("tr").last();
          const newRow = lastRow.clone();
          newRow.find("input:not([type='submit']), select:not([name*='[_weight]']), textarea").val("");
          const regex = /\[(\d+)\](?=\[[^\]]+\]$)/;
          // Update the names of inputs and selects in the new row
          newRow.find("input, select, textarea").each(function () {
            const field = $(this);
            const name = field.attr("name");
            if (name) {
              let newName = name.replace(regex, `[${index}]`);
              field.attr("name", newName);
            }
          });

          newRow.insertAfter(lastRow);
          removeWeightSelects();
          hideAddMore();
        }

        // Function to move row up
        function moveRowUp(row) {
          const prevRow = row.prev();
          if (prevRow.length) {
            row.insertBefore(prevRow);
            updateWeightSelects();
          }
        }

        // Function to move row down
        function moveRowDown(row) {
          const nextRow = row.next();
          if (nextRow.length) {
            row.insertAfter(nextRow);
            updateWeightSelects();
          }
        }

        // Function to focus cell above
        function focusCellAbove(cell) {
          const colIndex = cell.index();
          const prevRow = cell.closest("tr").prev();
          if (prevRow.length) {
            prevRow.find("td").eq(colIndex).find("input, select, textarea").focus();
          }
        }

        // Function to focus cell below
        function focusCellBelow(cell) {
          const colIndex = cell.index();
          const nextRow = cell.closest("tr").next();
          if (nextRow.length) {
            nextRow.find("td").eq(colIndex).find("input, select, textarea").focus();
          }
        }

        // Function to focus cell to the left
        function focusCellLeft(cell) {
          const prevCell = cell.prev();
          if (prevCell.length) {
            prevCell.find("input, select, textarea").focus();
          }
        }

        // Function to focus cell to the right
        function focusCellRight(cell) {
          const nextCell = cell.next();
          if (nextCell.length) {
            nextCell.find("input, select, textarea").focus();
          }
        }

        // Event listener for keyboard shortcuts
        table.on("keydown", function (e) {
          const focusedElement = $(":focus");
          const currentCell = focusedElement.closest("td");
          const currentRow = focusedElement.closest("tr");
          let totalRow = table.find("tr").length;
          if (!currentRow.length || !table.has(currentRow).length) return;

          switch (true) {
            case e.ctrlKey && e.key === "d":
              e.preventDefault();
              duplicateRow(currentRow, totalRow);
              break;
            case e.altKey && e.key === "n":
              e.preventDefault();
              addNewRow(totalRow);
              break;
            case e.ctrlKey && e.key === "ArrowUp":
              e.preventDefault();
              moveRowUp(currentRow);
              break;
            case e.ctrlKey && e.key === "ArrowDown":
              e.preventDefault();
              moveRowDown(currentRow);
              break;
            case e.key === "ArrowUp":
              e.preventDefault();
              focusCellAbove(currentCell);
              break;
            case e.key === "ArrowDown":
              e.preventDefault();
              focusCellBelow(currentCell);
              break;
            case e.key === "ArrowLeft":
              e.preventDefault();
              focusCellLeft(currentCell);
              break;
            case e.key === "ArrowRight":
              e.preventDefault();
              focusCellRight(currentCell);
              break;
          }
        });
      });
    },
  };
})(jQuery, Drupal, once);
