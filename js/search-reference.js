(function (Drupal, $, drupalSettings, once) {
  'use strict';
  Drupal.behaviors.search_reference = {
    attach: function (context) {
      $(once('search-reference', '.search-reference', context)).on("click", function () {
        let data = $(this).data();
        let settings = drupalSettings.field_reference[data['field_reference']];
        let search_key = settings.search_key || 'name';
        let currentLanguage = drupalSettings.path.currentLanguage || 'en';
        let mapLanguage = {
          'af': "af-ZA",
          'ar': "ar-SA",
          'ca': "ca-ES",
          'cs': "cs-CZ",
          'da': "da-DK",
          'de': "de-DE",
          'el': "el-GR",
          'en': "en-US",
          'es': "es-SP",
          'et': "et-EE",
          'eu': "eu-EU",
          'fa': "fa-IR",
          'fi': "fi-FI",
          'fr': "fr-FR",
          'he': "he-IL",
          'hr': "hr-HR",
          'hu': "hu-HU",
          'id': "id-ID",
          'it': "it-IT",
          'ja': "ja-JP",
          'ka': "ka-GE",
          'ko': "ko-KR",
          'ms': "ms-MY",
          'nl': "nl-NL",
          'nb': "nb-NO",
          'pl': "pl-PL",
          'pt': "pt-PT",
          'ro': "ro-RO",
          'ru': "ru-RU",
          'sk': "sk-SK",
          'sv': "sv-SE",
          'th': "th-TH",
          'tr': "tr-TR",
          'uk': "uk-UA",
          'ur': "ur-PK",
          'vi': "vi-VN",
          'pt-pt': "pt-PT",
          'pt-br': "pt-BR",
          'zh-hans': "zh-CN",
          'zh-hant': "zh-TW"
        }
        const locale = mapLanguage[currentLanguage] || 'en-US';
        let table = $(`<table
          class="table table-striped responsive-enabled caption-top"
          id="search-reference"
          data-toggle="table"
          data-search="true"
          data-click-to-select="true"
          data-locale="${locale}"
          data-pagination="true"
          data-id-field="id"
          data-url="${data['field_reference_url']}">
        <thead class="thead-light">
          <tr></tr>
        </thead>
        </table>`);
        let fields = [];
        settings.columns.forEach(function(column) {
          let th = $('<th>');
          Object.entries(column).forEach(function([key, value]) {
            if(key != 'title') {
              th.data(key, value);
            } else {
              th.html(value);
            }
          });
          let field = column.field;
          if(!['state', 'id', 'name'].includes(field)) {
            fields.push(field);
          }
          table.find('thead tr').append(th);
        });
        let parent = $(this).parent();
        let field_reference = data['field_reference'];
        let dialog = {
          title: data['bsTitle'],
          autoResize: true,
          dialogClass: 'search-dialog',
          width: '90%',
          modal: true,
          height: $(window).height() * 0.8,
          position: {my: "center"},
          buttons: [
            {
              text: "✔ " + Drupal.t('Select'),
              click: function () {
                let $table = $('#search-reference');
                getSelectedPasteClipboard($table.bootstrapTable('getSelections'), parent, field_reference, fields, search_key);
                // Emit event datafieldModalSearch then another javascript that can catch it.
                const event = new CustomEvent('datafieldModalSearch', {
                  detail: {
                    field: field_reference,
                    value: $table.bootstrapTable('getSelections')
                  }
                });
                document.dispatchEvent(event);
                $(this).dialog("close");
              }
            }
          ],
          close: function (event) {
            $(this).dialog('destroy').remove();
          },
          open: function (event, ui) {
            $(event.target).parent().css('background-color','white');
            let $table = $('#search-reference');
            $table.bootstrapTable('destroy').bootstrapTable();
          }
        };
        let showDialog = table.dialog(dialog);
      });

      function getSelectedPasteClipboard(selected, selector, column, fields, search_key) {
        let clipboard = '';
        selected.forEach(item => {
          let name = item[search_key] + ` (${item.id})`;
          let parts = [name];
          fields.forEach(field => {
            parts.push(item[field]);
          });
          clipboard += parts.join("\t") + "\n";
        });
        navigator.clipboard.writeText(clipboard).then(() => {
          selector.find('.paste-clipboard').data('field_reference', column).click();
        }).catch(err => {
          console.error('Error copy to clipboard:', err);
        });
      }
    }
  }
}(Drupal, jQuery, drupalSettings, once));
