/**
 * @file
 * Get datalistfield to table field widget.
 */

function use_number(node) {
  var empty_val = false;
  const value = node.value;
  if (node.value === '')
    empty_val = true;
  node.type = 'number';
  if (!empty_val)
    node.value = Number(value.replace(/,/g, ''));
}

function use_text(node) {
  var empty_val = false;
  const value = Number(node.value);
  if (node.value === '')
    empty_val = true;
  node.type = 'text';
  if (!empty_val)
    node.value = value.toLocaleString();
}
(function (Drupal, $, drupalSettings, once) {
  'use strict';
  Drupal.behaviors.datalistfield = {
    attach: function (context) {
      // Initialize the Quick Edit app once per page load.
      let data = [];
      $(once('datalist-field', '.datalistfield', context)).each(function () {
        let id = $(this).attr('list');
        if(drupalSettings.datalistfield[id] !== undefined){
          if ($.inArray(id, data) === -1) {
            data.push(id);
          }
        }
      });
      if(data.length){
        $.each(data, function (index, id) {
          if ($('datalist#' + id).length === 0) {
            let datalist = $('<datalist>').attr('id', id);
            let options = drupalSettings.datalistfield[id];
            $.each(options, function (value, label) {
              $('<option>').attr('value', value).text(label).appendTo(datalist);
            });
            $('body').append(datalist);
          }
        });
      }
    }
  };
}(Drupal, jQuery, drupalSettings, once));
