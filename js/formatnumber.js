/**
 * @file
 * Get data field to number format field widget.
 */

function use_telphone(input) {
  input.addEventListener('input', function () {
    let separator = input.getAttribute("separator") || ' ';
    let step = parseInt(input.getAttribute("step"), 10) || 2;
    let value = input.value.replace(new RegExp(`\\${separator}`, 'g'), '');
    let formattedValue = value.match(new RegExp(`.{1,${step}}`, 'g'))?.join(separator) || '';
    input.value = formattedValue;
  });
}

function use_number(input) {

  if (!input.value.trim()) {
    input.type = 'number';
    return;
  }
  const value = input.value.replace(/[^\d.-]/g, '');
  input.type = 'number';
  input.value = value;
}

function use_text(input) {
  if (!input.value.trim()) {
    input.type = 'text';
    return;
  }

  const value = Number(input.value);
  if (!isNaN(value)) {
    const formattedValue = value.toLocaleString(navigator.language);
    input.type = 'text';
    input.value = formattedValue;
  } else {
    input.type = 'text';
  }
}

(function (Drupal, $, drupalSettings, once) {
  'use strict';
  Drupal.behaviors.formatNumber = {
    attach: function (context) {
      $(once('text-format-number', '.text-format-number', context)).each(function () {
        let language = drupalSettings.auto_numeric.language;
        if (language) {
          let option = AutoNumeric.getPredefinedOptions()[language];
          let newOption = Object.assign({}, option);
          newOption.currencySymbol = '';
          new AutoNumeric(this, newOption);
        }
      });
    }
  };
}(Drupal, jQuery, drupalSettings, once));
