/**
 * @file
 * Custom JS for the JSON table field formatter.
 */
(function (Drupal, $, once) {
  'use strict';

  /**
   * Attach behavior for JSON Fields.
   *
   * @type {Drupal~behavior}
   */

  /**
   * Attach behavior for JSON Fields.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.json_table = {
    attach: function (context) {
      // Initialize the Quick Edit app once per page load.
      $(once('json-table-field', '.field--type-json.field--widget-json-table-widget', context)).each(function () {
        $(once('table-add-row', '.add-row', context)).on('click', function () {
          let tbody = $(this).closest('.json-table-wrapper').find('table tbody');
          let tr = tbody.children().first().clone();
          tbody.append(tr);
          convertTable2Json($(this).closest('.json-table-wrapper').find('table tbody'));
          keyupInputTable();
        });
        $(once('table-add-col', '.add-col', context)).on('click', function () {
          $(this).closest('.json-table-wrapper').find('table tbody tr').append('<td><input class="form-control form-element form-text"/></td>');
          $(this).closest('.json-table-wrapper').find('table thead tr').append('<th>' + Drupal.t('Value') + '</th>');
          convertTable2Json($(this).closest('.json-table-wrapper').find('table tbody'));
          keyupInputTable();
        });
        $(once('table-remove-row', '.remove-row', context)).on('click', function () {
          $(this).closest('.json-table-wrapper').find('table tbody tr:last').remove();
          convertTable2Json($(this).closest('.json-table-wrapper').find('table tbody'));
        });
        $(once('table-remove-col', '.remove-col', context)).on('click', function () {
          $(this).closest('.json-table-wrapper').find('table tbody tr td:last-child').remove();
          $(this).closest('.json-table-wrapper').find('table thead tr th:last-child').remove();
          convertTable2Json($(this).closest('.json-table-wrapper').find('table tbody'));
        });
        keyupInputTable();
        function getHeaderArray(that){
          var header = [];
          let $table = that.closest('table'),
            header_associative = $table.data('associative');
          if(header_associative == '1'){
            // This loop is not very efficient. It crashes browser.
            $table.find('thead th').each(function (key,value) {
              header.push(value.innerHTML);
            });
          }
          return header;
        }
        function convertTable2Json(that) {
          let $textarea = that.closest('.form-item').find('textarea.json-table');

          var table = [];
          var header = getHeaderArray(that);
          that.closest('table').find('tbody tr').each(function (row, tr) {

            let rows = $(this).find('td');
            table[row] = {};
            $.each(rows, function (col, value) {
              let associative = col;
              if(header[col] !== undefined){
                associative = header[col];
              }
              table[row][associative] = $(this).find('input,select,textarea').val();
            });
          });
          const result = {};
          table.forEach(item => {
            let key = item[0];
            delete item[0];
            if(Object.keys(item).length == 1){
              result[key] = item[1];
            } else {
              result[key] = Object.values(item);
            }
          });
          $textarea.val(JSON.stringify(result));
        }
        function keyupInputTable(){
          $('.json-table input').on('change keyup', function (e) {
            convertTable2Json($(this))
          });
        }
      });
    }
  };
}(Drupal, jQuery, once));
