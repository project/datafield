(function (Drupal, $, drupalSettings, once) {
  Drupal.behaviors.apexCharts = {
    attach: function (context, settings) {
      $.each(settings.apexCharts, function (selector) {
        $(once('apex', selector, context)).each(function () {
          // Check if table contains expandable hidden rows.
          let dataTable = drupalSettings.apexCharts[selector]['data'];
          let series = dataTable[0].map((name, index) => ({
            name: name,
            data: dataTable.slice(1).map(row => row[index])
          }));
          let categories = series.shift();
          let options = drupalSettings.apexCharts[selector]['options'];
          options.chart.height = drupalSettings.apexCharts[selector].chart_height || 300;
          options.title = {
              text: drupalSettings.apexCharts[selector].title || '',
              align: 'left'
          }
          options.series = series;
          if (options.xaxis) {
            options.xaxis.categories = categories.data;
          } else {
            options.xaxis = {categories: categories.data};
          }

          let monoChart = ['pie', 'donut', 'polarArea', 'radialBar'];
          if (monoChart.includes(options.chart.type)) {
            document.querySelector(selector).classList.add('row');
            document.querySelector(selector).classList.add('w-100');
            series.forEach((item, index) => {
              let newDiv = document.createElement('div');
              newDiv.id = selector.replace('#','') + index;
              newDiv.classList.add('col-4');
              document.querySelector(selector).appendChild(newDiv);
              options.series = item.data;
              options.title.text = item.name;
              options.labels = categories.data;
              let chart = new ApexCharts(newDiv, options);
              chart.render();
            });
          } else {
            let chart = new ApexCharts(document.querySelector(selector), options);
            chart.render();
          }
        });
      });
    }
  };

}(Drupal, jQuery, drupalSettings, once));
