/**
 * @file
 * Fill all field widget.
 */

(function (Drupal, $, drupalSettings, once) {
  'use strict';
  Drupal.behaviors.datafield_autocomplete = {
    attach: function (context) {
      $(context).find('.datafield-autocomplete').on('autocompleteselect', function (event, data) {
        let dataFields = Object.entries(data.item.data);
        let field = data.item.field;
        let delta = data.item.delta;
        dataFields.forEach(([key, value]) => {
          let selector = `${field}[${delta}][${key}]`;
          $(`[name='${selector}']`).val(value);
        });
      });
    }
  };
}(Drupal, jQuery, drupalSettings, once));
