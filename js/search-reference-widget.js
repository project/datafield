(function (Drupal, $, drupalSettings, once) {
  'use strict';
  Drupal.behaviors.search_reference_widget = {
    attach: function (context) {
      $(once('search-reference-widget', '.search-reference-widget', context)).on("click", function () {
        let data = $(this).data();
        let field_reference = data['reference'];
        let settings = drupalSettings.datafield[field_reference];
        let search_key = settings.search_key || 'name';
        let currentLanguage = drupalSettings.path.currentLanguage || 'en';
        let mapLanguage = {
          'af': "af-ZA",
          'ar': "ar-SA",
          'ca': "ca-ES",
          'cs': "cs-CZ",
          'da': "da-DK",
          'de': "de-DE",
          'el': "el-GR",
          'en': "en-US",
          'es': "es-SP",
          'et': "et-EE",
          'eu': "eu-EU",
          'fa': "fa-IR",
          'fi': "fi-FI",
          'fr': "fr-FR",
          'he': "he-IL",
          'hr': "hr-HR",
          'hu': "hu-HU",
          'id': "id-ID",
          'it': "it-IT",
          'ja': "ja-JP",
          'ka': "ka-GE",
          'ko': "ko-KR",
          'ms': "ms-MY",
          'nl': "nl-NL",
          'nb': "nb-NO",
          'pl': "pl-PL",
          'pt': "pt-PT",
          'ro': "ro-RO",
          'ru': "ru-RU",
          'sk': "sk-SK",
          'sv': "sv-SE",
          'th': "th-TH",
          'tr': "tr-TR",
          'uk': "uk-UA",
          'ur': "ur-PK",
          'vi': "vi-VN",
          'pt-pt': "pt-PT",
          'pt-br': "pt-BR",
          'zh-hans': "zh-CN",
          'zh-hant': "zh-TW"
        }
        const locale = mapLanguage[currentLanguage] || 'en-US';
        let table = $(`<table
          class="table table-striped responsive-enabled caption-top table"
          id="search-reference"
          data-toggle="table"
          data-search="true"
          data-click-to-select="true"
          data-locale="${locale}"
          data-pagination="true"
          data-id-field="id"
          data-url="${data['url']}">
        <thead class="thead-light">
          <tr></tr>
        </thead>
        </table>`);
        let fields = [];
        settings.columns.forEach(function(column) {
          let th = $('<th>');
          Object.entries(column).forEach(function([key, value]) {
            if(key != 'title') {
              th.data(key, value);
            } else {
              th.html(value);
            }
          });
          let field = column.field;
          if(!['state', 'id', 'name'].includes(field)) {
            fields.push(field);
          }
          table.find('thead tr').append(th);
        });
        let inputReference = $(this).closest('.form-item').find('input');
        let row = $(this).closest('tr');
        let dialog = {
          title: data['bsTitle'],
          autoResize: true,
          dialogClass: 'search-dialog',
          width: '90%',
          modal: true,
          height: $(window).height() * 0.8,
          position: {my: "center"},
          buttons: [
            {
              text: "✔ " + Drupal.t('Select'),
              click: function () {
                let $table = $('#search-reference');
                let selected = $table.bootstrapTable('getSelections');
                if (selected[0]) {
                  inputReference.val(selected[0][search_key] + ' (' + selected[0].id + ')');
                  $.each(selected[0], function (key, value) {
                    key = key.replace('field_', '');
                    if (key != search_key) {
                      row.find('input[name^="' + field_reference + '["][name$="][' + key + ']"], select[name^="' + field_reference + '["][name$="][' + key + ']"]').each(function () {
                        $(this).val(value);
                      });
                    }
                  });
                  // Emit event datafieldEntityReferenceSearch then another javascript that can catch it.
                  const event = new CustomEvent('datafieldEntityReferenceSearch', {
                    detail: {
                      field: field_reference,
                      value: selected[0]
                    }
                  });
                  document.dispatchEvent(event);
                }
                $(this).dialog("close");
              }
            },
          ],
          close: function (event) {
            $(this).dialog('destroy').remove();
          },
          open: function (event, ui) {
            $(event.target).parent().css('background-color','white');
            let $table = $('#search-reference');
            $table.bootstrapTable('destroy').bootstrapTable();
          }
        };
        let showDialog = table.dialog(dialog);
      });

    }
  }
}(Drupal, jQuery, drupalSettings, once));
