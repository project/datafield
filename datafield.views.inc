<?php

/**
 * @file
 * Provide Views data for the datafield module.
 *
 * @ingroup views_module_handlers
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data().
 */
function datafield_field_views_data(FieldStorageConfigInterface $field_storage) {
  $data = views_field_default_views_data($field_storage);
  $entity_manager = \Drupal::entityTypeManager();
  $field_name = $field_storage->getName();
  $columns = $field_storage->getSetting('columns');
  $field_settings = $field_storage->getSetting('field_settings');
  $types_with_relationships = ['file', 'image', 'entity_reference'];

  foreach ($data as $table_name => $table_data) {
    foreach ($columns as $name => $column) {
      $field_key = $field_name . '_' . $name;
      $data[$table_name][$field_key]['field'] = [
        'id' => 'standard',
        'field_name' => $field_name,
        'property' => $name,
      ];

      // Build views relationships if applicable.
      if (in_array($column['type'], $types_with_relationships)) {
        $target_type = $field_settings[$name]['entity_reference_type'] ?? '';
        if (!empty($target_type) && $entity_manager->hasHandler($target_type, 'views_data')) {
          $target_entity_type = $entity_manager->getDefinition($field_settings['target_type']);
          $entity_type_id = $field_storage->getTargetEntityTypeId();
          $entity_type = $entity_manager->getDefinition($entity_type_id);
          $target_base_table = $entity_manager->getHandler($target_type, 'views_data')->getViewsTableForEntityType($target_entity_type);
          $args = ['@label' => $target_entity_type->getLabel(), '@field_name' => $field_key];

          $data[$table_name][$field_key]['relationship'] = [
            'title' => t('@label referenced from @field_name', $args),
            'group' => $entity_type->getLabel(),
            'id' => 'standard',
            'base' => $target_base_table,
            'entity type' => $target_type,
            'base field' => $target_entity_type->getKey('id'),
            'relationship field' => $field_key,
            'label' => t('@field_name: @label', $args),
          ];

          // Provide a reverse relationship for the referenced entity type.
          $pseudo_field_name = 'reverse__' . $entity_type_id . '__' . $field_key;
          $table_mapping = $entity_manager->getStorage($entity_type_id)->getTableMapping();
          $args['@entity'] = $entity_type->getLabel();
          $args['@label'] = $target_entity_type->getSingularLabel();
          $data[$target_base_table][$pseudo_field_name]['relationship'] = [
            'title' => t('@entity using @field_name', $args),
            'label' => t('@field_name', ['@field_name' => $field_key]),
            'group' => $target_entity_type->getLabel(),
            'help' => t('Relate each @entity with a @field_name set to the @label.', $args),
            'id' => 'entity_reverse',
            'base' => $entity_manager->getHandler($entity_type_id, 'views_data')->getViewsTableForEntityType($entity_type),
            'entity_type' => $entity_type_id,
            'base field' => $entity_type->getKey('id'),
            'field_name' => $field_name,
            'field table' => $table_mapping->getDedicatedDataTableName($field_storage),
            'field field' => $field_key,
          ];
        }
      }
    }
  }
  return $data;
}
